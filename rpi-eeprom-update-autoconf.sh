#!/bin/bash
cat << EOF
-----------------------------------------------------------
install rpi-eeprom-update for Raspberry Pi 4 (Ubuntu 20.04)
-----------------------------------------------------------
EOF

while true; do
  read -p "first 'sudo apt full-upgrade -y'. Do you wish to install this program?" yn
  case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
  esac
done

sudo apt update -y
sudo apt full-upgrade -y

curl -O http://ports.ubuntu.com/pool/universe/r/raspberrypi-userland/libraspberrypi0_0~20200520+git2fe4ca3-0ubuntu2_arm64.deb
sudo apt install ./libraspberrypi0_0~20200520+git2fe4ca3-0ubuntu2_arm64.deb
curl -O http://ports.ubuntu.com/pool/universe/r/raspberrypi-userland/libraspberrypi-bin_0~20200520+git2fe4ca3-0ubuntu2_arm64.deb 
sudo apt install ./libraspberrypi-bin_0~20200520+git2fe4ca3-0ubuntu2_arm64.deb

sudo add-apt-repository ppa:waveform/eeprom -y
sudo apt install rpi-eeprom -y

sudo systemctl stop rpi-eeprom-update.service
sudo systemctl disable rpi-eeprom-update.service

sudo wget -O /lib/firmware/raspberrypi/bootloader/stable/pieeprom-2021-01-16.bin https://github.com/raspberrypi/rpi-eeprom/raw/master/firmware/stable/pieeprom-2021-01-16.bin

sudo sudo rpi-eeprom-update -a

cat << EOF
---------------------------------

Reboot require.

---------------------------------
EOF
