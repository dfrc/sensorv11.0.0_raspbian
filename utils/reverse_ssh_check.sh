#!/bin/bash

ssh -O check RSSH
ret=$?

if [ $ret -gt 0 ]
then
  ssh -fN RSSH
fi
