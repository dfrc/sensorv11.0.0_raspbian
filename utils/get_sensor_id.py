#!/bin/python3

import yaml

with open('/opt/lba_sensor/jar/java-sensor-configuration.yaml') as stream:
  conf = yaml.load(stream, Loader=yaml.CLoader)

sensorId = conf['operatingSensorIds']

print(sensorId)
