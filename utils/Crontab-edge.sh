#!/bin/bash
set -e

#echo "*/1 * * * * /opt/utils/event_backup.sh" > ~/mycron
#echo "*/0 23 * * * /opt/utils/client_download.sh" >> ~/mycron
#echo "*/1 * * * * /opt/utils/reverse_ssh_check.sh" >> ~/mycron
echo "0 2 * * * /opt/utils/RemoveOldFin.sh" >> ~/mycron
echo "1 15 * * * /opt/utils/dailyreboot.sh" >> ~/mycron
#echo "4 16 * * * java -jar /opt/utils/statusmessageip-v1.0.0.jar --type sensor" >> ~/mycron
echo "3 16 * * * /opt/utils/remove_old_log.sh" >> ~/mycron
#echo "2 16 * * * java -jar /opt/utils/softwareversion-1.1.jar" >> ~/mycron
#echo "1 16 * * * java -jar /opt/utils/freediskspace-1.0.0.jar" >> ~/mycron
#echo "0 16 * * * java -cp /opt/utils/modemdetails-2.1.jar Main" >> ~/mycron
#echo "*/30 * * * * java -jar /opt/utils/sensortemperature-2.0.jar" >> ~/mycron

#echo "*/10 * * * * java -jar /opt/utils/sensorrestart-3.2.4.jar" >> ~/mycron
#echo "*/120 * * * * java -jar /opt/utils/networkconnection-1.2.jar true" >> ~/mycron
#echo "*/90 * * * * java -jar /opt/utils/freememory-1.0.0.jar" >> ~/mycron
#echo "*/30 * * * * java -cp /opt/utils/networkspeed.jar Main" >> ~/mycron
#network status is only working with Sixfab board
#echo "*/45 * * * * java -cp /opt/utils/networkstatus-1.1.jar Main" >> ~/mycron
echo "*/10 * * * * /opt/utils/remote_control_command /opt/lba_sensor/jar/java-sensor-configuration.yaml "  >> ~/mycron

#
# The following line should be used only for sensors with LAT Hat
#echo "*/10 * * * * java -Djava.util.logging.config.file=/opt/utils/conf_sensorrestartLTE/logging.properties -jar /opt/utils/sensorrestartLTE-4.2.1.jar" >> ~/mycron

echo "0 1 * * * /usr/bin/sudo find  /opt/lba_sensor/jar/*.fin -mtime +7 -exec rm -f {} \;" >> ~/mycron

# no captive portal in the offical version
#echo "0 */12 * * * /opt/utils/LBA.CaptivePortal.restart_every_12h.sh" >> ~/mycron

# the following like should be removed if LED is not installed
echo "* * * * * /opt/lba_led/led_control.sh" >> ~/mycron


cat ~/mycron
crontab ~/mycron
rm ~/mycron
# java -jar /opt/utils/sendmessage-1.0.0.jar "Crontab had been updated"
