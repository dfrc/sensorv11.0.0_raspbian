#!/usr/bin/env python3
import argparse
import fileinput
import getpass
import logging as logger
import os
import pathlib
import sys
#from envbash import load_envbash

# logger.basicConfig(
#     format='[%(asctime)s %(levelname)s] %(message)s',
#     filename='update_reverse_ssh.log',
#     level=logger.DEBUG
# )
#current_path = pathlib.Path(__file__).parent.absolute()
#env = os.path.join(current_path, '.env')
#load_envbash(env)

DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"
DEFAULT_EVENTS_FILE_PATH = "/opt/lba_sensor/jar"
LBA_SENSOR_CONFIG_FILE = "java-sensor-configuration.yaml"
SSH_CONFIG_FILE_PATH = '/home/{}/.ssh/config'.format(getpass.getuser())
COLORS = {'RED': '1;31', 'GREEN': '1;32', 'YELLOW': '1;33', 'BLUE': '1;36'}


class UpdateReverseSSH:
    def __init__(self):
        # logger.info("UpdateReverseSSH.__init__()")
        # logger.debug("UpdateReverseSSH.__init__() | python={}".format(sys.executable))
        parser = argparse.ArgumentParser(description='Utility to update reverse ssh config file')
        parser.add_argument('--RemoteForward', default='False', type=str, help='Update SSH RemoteForward value')
        self.args = parser.parse_args()
        self.sensor_id = None
        self.reverse_hostname = 'RSSH'

    def process_args(self):
        # logger.info("UpdateReverseSSH.process_args()")
        self.args.RemoteForward = True if self.args.RemoteForward == 'True' else False

        try:
            with open('{}/{}'.format(DEFAULT_EVENTS_FILE_PATH, LBA_SENSOR_CONFIG_FILE)) as infile:
                for line in infile:
                    if 'operatingSensorIds' in line:
                        self.sensor_id = line.strip('\n').split(': ')[1]
                        self.sensor_id = self.sensor_id.strip('"') if '"' in self.sensor_id else self.sensor_id
        except FileNotFoundError:
            self.sensor_id = -1

    def execute(self):
        # logger.info("UpdateReverseSSH.execute()")

        if self.args.RemoteForward and self.sensor_id:
            old_value = ''
            new_value = ''

            reverse_found = False
            with fileinput.FileInput(SSH_CONFIG_FILE_PATH, inplace=True, backup='.bak') as file:
                for line in file:
                    config = line.strip('\n').split(' ')
                    if 'Host' in line and self.reverse_hostname in line:
                        reverse_found = True
                    else:
                        if 'RemoteForward' in line and reverse_found:
                            old_value = line.strip('\n')

                            remote_forward_index = config.index('RemoteForward')
                            remote_forward_port_index = remote_forward_index + 1  # next item is the port
                            config[remote_forward_port_index] = config[remote_forward_port_index][0] + self.sensor_id
                            line = ' '.join(config)
                            reverse_found = False

                            new_value = line.strip('\n')

                    print(line.strip('\n'))

            message = "{} file updated.\n'{}' => '{}'".format(SSH_CONFIG_FILE_PATH, old_value, new_value)
        else:
            message = "No changes."

        return message

    def run(self, *args, **kwargs):
        # logger.info("UpdateReverseSSH.run()")
        self.process_args()

        if self.sensor_id != -1:
            output = self.execute()
            self.colored_print(output)
        else:
            self.colored_print("Error: File={}/{} does not exist.".format(
                DEFAULT_EVENTS_FILE_PATH, LBA_SENSOR_CONFIG_FILE), COLORS['RED']
            )

    @staticmethod
    def colored_print(message, color=None):
        # logger.info("UpdateReverseSSH.colored_print()")
        if color:
            sys.stdout.write('\x1b[{}m{}\x1b[0m\n'.format(color, message))
        else:
            print(message)


if __name__ == '__main__':
    # logger.info("update_reverse_ssh.py: __main__()")
    sys.exit(UpdateReverseSSH().run(*sys.argv))
