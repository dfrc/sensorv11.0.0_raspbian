#!/bin/bash

# Remove comment for EDGE sensor 
# sudo cp /opt/lba_sensor/jar/*.fin /data/sites/site1/raw/
# sudo chmod 666 /data/sites/site1/raw/*.fin
#

server="ubuntu@15.164.1.120"
ssh_key="/home/pi/.ssh/DMZ.pem"
sudo rsync -e -q -rave "ssh -p 22 -i ${ssh_key}" /opt/lba_sensor/jar/*.fin ${server}:/data/events --remove-source-files

