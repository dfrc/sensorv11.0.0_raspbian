#!/bin/bash

sudo rm -r /opt/download/*

server="ubuntu@15.164.1.120"
ssh_key="~/.ssh/DMZ.pem"

sensor_id=$('/opt/utils/get_sensor_id.py')

# sudo rsync -rave "ssh -i ${ssh_key}" ${server}:/opt/lba_sensor/upload/${sensor_id}/ /opt/download/
sudo rsync -rave "ssh -i ${ssh_key}" ${server}:/opt/download/ /opt/download/

