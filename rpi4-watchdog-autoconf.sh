#!/bin/bash
cat << EOF
----------------------------------------------------
 install watchdog for Raspberry Pi 4 (Ubuntu 20.04)
                                         2021-02-03
----------------------------------------------------
EOF

echo " *** Checking device tree ..."
if grep -Fxq dtparam=watchdog=on /boot/firmware/usercfg.txt
then
  echo " - Watchdog has been added to the device tree"
else
  echo " - Add the watchdog to the device tree"
  echo "dtparam=watchdog=on" | sudo tee -a /boot/firmware/usercfg.txt
fi

echo ""

echo " *** Checking watchdog ..."
if ls /dev/watchdog* | grep -Fq watchdog
then
  echo " - Watchdog device found"
else
  cat << EOF
-----------------------------------------

 After rebooting, run this script again.

-----------------------------------------
EOF
  exit 1
fi

echo ""

echo " *** Start installing watchdog program ..."
sudo apt update -y
sudo apt install watchdog -y

#echo "max-load-1 = 24" | sudo tee /etc/watchdog.conf
#echo "min-memory = 1" | sudo tee -a /etc/watchdog.conf
echo "watchdog-device = /dev/watchdog" | sudo tee -a /etc/watchdog.conf
echo "realtime = yes" | sudo tee -a /etc/watchdog.conf
echo "priority = 1" | sudo tee -a /etc/watchdog.conf
echo "watchdog-timeout = 14" | sudo tee -a /etc/watchdog.conf

sudo systemctl stop watchdog
sudo systemctl enable watchdog
sudo systemctl start watchdog
sudo systemctl status watchdog

cat << EOF
------------------

 Watchdog ready.

------------------
EOF
