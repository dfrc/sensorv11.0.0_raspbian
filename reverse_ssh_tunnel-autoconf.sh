#!/bin/bash
echo "Reverse SSH Tunnel Auto Config. 2020-OCT-04"

mkdir ~/.ssh/

cp reverse_ssh_tunnel/config ~/.ssh/
cp reverse_ssh_tunnel/RSSH.pem ~/.ssh/
chmod 0400 ~/.ssh/RSSH.pem
chown pi ~/.ssh/RSSH.pem
mkdir ~/.ssh/sockets

ssh-keygen -R 3.34.242.51
ssh-keyscan -H 3.34.242.51  >> ~/.ssh/known_hosts

echo "start$ ssh -fN RSSH"
echo "check$ ssh -O check RSSH"
echo " exit$ ssh -O exit RSSH"
echo "OK"
