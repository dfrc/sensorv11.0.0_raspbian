#!/bin/sh
YELLOW='\033[1;33m'
RED='\033[0;31m'
BLUE='\033[1;34m'
SET='\033[0m'

echo "${YELLOW}--Sensor installation on pi system--${SET}"
sudo mkdir /opt/lba_sensor
sudo cp -r esp/lba_sensor /opt/
sudo ln -s /opt/lba_sensor/jar/java-sensor-2.2.8.jar /opt/lba_sensor/java-sensor-latest
ls -l /opt/lba_sensor/

echo "${YELLOW}--Create service--${SET}"
sudo cp esp/lba-sensor.service /etc/systemd/system



sudo systemctl daemon-reload
sudo systemctl enable lba-sensor.service
sudo systemctl start lba-sensor

