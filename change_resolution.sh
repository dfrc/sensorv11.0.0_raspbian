#!/bin/sh
# WARNING! It works only on Raspberry Pi
# Check documentation
# https://www.raspberrypi.org/documentation/configuration/config-txt/video.md

sudo sh -c 'echo hdmi_force_hotplug=1 >> /boot/firmware/usercfg.txt'
sudo sh -c 'echo hdmi_group=2 >> /boot/firmware/usercfg.txt'
sudo sh -c 'echo hdmi_mode=83 >> /boot/firmware/usercfg.txt'