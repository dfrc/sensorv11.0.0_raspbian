import serial              
from time import sleep
import time
import sys
import datetime
import os
import csv
import re
import requests
from requests.auth import HTTPBasicAuth
import json
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Read config file
file_path = os.path.dirname(os.path.abspath(__file__))
config_path = file_path + "/lba_gps_configuration.json"
with open(config_path) as f:
    config_json = json.load(f)
keep_days = config_json['keep_days']
print_debug = config_json['print_debug']
send_to_cloud = config_json['send_sensor_location_to_cloud']
send_interval = config_json['send_sensor_location_interval']
server_url = config_json['realtime_server']

ser = serial.Serial ("/dev/ttyAMA0")
nmea_position_fix = None
nmea_time = None
lat = None
longi = None
nmea_ns_indicator = None
nmea_ew_indicator = None
nmea_altitude = None
nmea_units = None
nmea_satellites_used = None
nmea_checksum = None
gpgsa_pdop = None
gprmc_ground_speed = None
gprmc_course_over_ground = None
gprmc_date = None
gprmc_status = None
gprmc_ground_speed = None
gprmc_course_over_ground = None
gprmc_checksum = None

output_directory = "/opt/lba_gps"

def convert_to_degrees(raw_value):
    decimal_value = raw_value/100.00
    degrees = int(decimal_value)
    mm_mmmm = (decimal_value - int(decimal_value))/0.6
    position = degrees + mm_mmmm
    position = "%.5f" %(position)
    return position

try:
    before_time = time.time() # For saving raw gps csv file
    before_time2 = time.time() # For sending gps data via API call
    while True:
        try:
            gprmc_str = "GPRMC"
            gpgga_str = "GPGGA"
            gpgsa_str = "GPGSA"
            received_data = str(ser.readline()) #read NMEA string received
            GPRMC_data_available = received_data.find(f"${gprmc_str},")   #check for NMEA GPRMC string
            GNRMC_data_available = received_data.find("$GNRMC,")   #check for NMEA GNRMC string
            GPGGA_data_available = received_data.find(f"${gpgga_str},")   #check for NMEA GPGGA string
            GNGGA_data_available = received_data.find("$GNGGA,")   #check for NMEA GNGGA string
            GPGSA_data_available = received_data.find(f"${gpgsa_str},")
            GNGSA_data_available = received_data.find("$GNGSA,")

            if GPRMC_data_available < GNRMC_data_available:
                gprmc_str = "GNRMC"
    
            if GPGGA_data_available < GNGGA_data_available:
                gpgga_str = "GNGGA"

            if GPGSA_data_available < GNGSA_data_available:
                gpgga_str = "GNGSA"

            if GPGSA_data_available > 0 or GNGSA_data_available > 0:
                GPGSA_buffer = received_data.split(f"${gpgsa_str},",1)[1]
                NMEA_GPGSA_buff = GPGSA_buffer.split(',')
                if print_debug:
                    print(f"{gpgsa_str} info: {NMEA_GPGSA_buff}")
                gpgsa_pdop = NMEA_GPGSA_buff[14]
                if gpgsa_pdop == "":
                    gpgsa_pdop = None
                else:
                    gpgsa_pdop = float(NMEA_GPGSA_buff[14])

            if GPRMC_data_available > 0 or GNRMC_data_available > 0:
                GPRMC_buffer = received_data.split(f"${gprmc_str},",1)[1]
                NMEA_GPRMC_buff = GPRMC_buffer.split(',')
                if print_debug:
                    print(f"{gprmc_str} info: {NMEA_GPRMC_buff}")
                gprmc_status = NMEA_GPRMC_buff[1]
                gprmc_ground_speed = NMEA_GPRMC_buff[6]
                gprmc_course_over_ground = NMEA_GPRMC_buff[7]
                gprmc_date = NMEA_GPRMC_buff[8]
                gprmc_checksum = NMEA_GPRMC_buff[-1]

                if gprmc_status == '':
                    gprmc_status = None
                if gprmc_ground_speed == '':
                    gprmc_ground_speed = None
                else:
                    # Converting to Km/h
                    gprmc_ground_speed = round(float(gprmc_ground_speed) * 1.852, 6)
                if gprmc_course_over_ground == '':
                    gprmc_course_over_ground = None
                else:
                    gprmc_course_over_ground = float(gprmc_course_over_ground)
                if gprmc_date == '':
                    gprmc_date = None
                else:
                    gprmc_date = datetime.datetime.strptime(gprmc_date, "%d%m%y")
                if gprmc_checksum == '':
                    gprmc_checksum = None
                else:
                    gprmc_checksum = gprmc_checksum.replace("'",'').replace("\\r",'').replace("\\n",'')

            if GPGGA_data_available > 0 or GNGGA_data_available > 0:
                GPGGA_buffer = received_data.split(f"${gpgga_str},",1)[1]  #store data coming after “$GPGGA,” string
                NMEA_buff = GPGGA_buffer.split(',')
                nmea_time = NMEA_buff[0]   
                nmea_latitude = NMEA_buff[1]
                nmea_ns_indicator = NMEA_buff[2]         
                nmea_longitude = NMEA_buff[3]
                nmea_ew_indicator = NMEA_buff[4]
                nmea_position_fix = NMEA_buff[5]
                nmea_satellites_used = NMEA_buff[6]
                nmea_altitude = NMEA_buff[8]
                nmea_units = NMEA_buff[9]
                nmea_checksum = NMEA_buff[-1]
                if print_debug:
                    print(f"{gpgga_str} info: {NMEA_buff}")
                if nmea_time == '':
                    nmea_time = None
                else:
                    first_part = nmea_time.split('.')[0]
                    second_part = nmea_time.split('.')[1]
                    utc_hour = int(first_part[:2])
                    utc_minute = int(first_part[2:4])
                    utc_second = int(first_part[4:])
                    utc_microsecond = int(second_part) * 10**(6 - len(second_part))

                    # Fetch date info from GPRMC data if it's not available, it will be displayed as 1970-01-01
                    if gprmc_date == None:
                        current_year = 1970
                        current_month = 1
                        current_day = 1
                    else:
                        current_year = gprmc_date.year
                        current_month = gprmc_date.month
                        current_day = gprmc_date.day
                    nmea_time = datetime.datetime(year=current_year, month=current_month, day=current_day, hour=utc_hour, minute=utc_minute, second=utc_second, microsecond=utc_microsecond)

                if nmea_latitude == '':
                    lat = None
                else:
                    lat = float(nmea_latitude)
                    lat = convert_to_degrees(lat)
                if nmea_longitude == '':
                    longi = None
                else:
                    longi = float(nmea_longitude)
                    longi = convert_to_degrees(longi)
                if nmea_ns_indicator == '':
                    nmea_ns_indicator = None
                if nmea_ew_indicator == '':
                    nmea_ew_indicator = None
                if nmea_position_fix == '':
                    nmea_position_fix = None
                else:
                    nmea_position_fix = int(nmea_position_fix)
                if nmea_satellites_used == '':
                    nmea_satellites_used = None
                else:
                    nmea_satellites_used = int(nmea_satellites_used)
                if nmea_altitude == '':
                    nmea_altitude = None
                else:
                    nmea_altitude = float(nmea_altitude)
                if nmea_units == '':
                    nmea_units = None
                if nmea_checksum == '':
                    nmea_checksum = None
                else:
                    nmea_checksum = nmea_checksum.replace("'",'').replace("\\r",'').replace("\\n",'')

                body = f"\nNMEA Time: {nmea_time}"
                body += f"\nLatitude: {lat}, Longitude: {longi}"
                body += f"\ngpgsa_pdop: {gpgsa_pdop}"
                body += f"\nns_indicator: {nmea_ns_indicator}"
                body += f"\new_indicator: {nmea_ew_indicator}"
                body += f"\nposition_fix: {nmea_position_fix}"
                body += f"\nsatellites_used: {nmea_satellites_used}"
                body += f"\naltitude: {nmea_altitude}"
                body += f"\nunit: {nmea_units}"
                body += f"\ngpgga_checksum: {nmea_checksum}"
                body += f"\ngprmc_status: {gprmc_status}"
                body += f"\ngprmc_ground_speed: {gprmc_ground_speed}"
                body += f"\ngprmc_course_over_ground: {gprmc_course_over_ground}"
                body += f"\ngprmc_checksum: {gprmc_checksum}\n"
                if print_debug:
                    print(body)
                else:
                    if nmea_position_fix != 0 and gprmc_status == "A":
                        print(f"NMEA Time: {nmea_time}")
                        print(f"GPS module received vaild GPS data from {nmea_satellites_used} satellites.")
                    else:
                        print(f"NMEA Time: {nmea_time}")
                        print(f"GPS data is invalid or satellites are not reachable.")        

            # Output GPS data into a csv file under /opt/lba_gps every 1 second.
            current_time = time.time()
            if current_time >= before_time + 1:
                before_time = current_time
                # Fetch sensor id from /opt/lba_sensor/jar/java-sensor-configuration.yaml
                java_sensor_configuration_dir = "/opt/lba_sensor/jar/java-sensor-configuration.yaml"
                with open(java_sensor_configuration_dir, "r") as f:
                    java_sensor_config = f.readlines()
                config_sensor_line = [x for x in java_sensor_config if 'SensorId' in x][0]
                sensor_id_str = re.findall("[0-9]{3,5}", config_sensor_line)
                if sensor_id_str == []:
                    sensor_id = 0
                else:
                    sensor_id = int(sensor_id_str[0])

                if nmea_position_fix != 0 and gprmc_status == 'A':
                    valid_flag = True
                else:
                    valid_flag = False
                if nmea_time == None:
                    nmea_timestamp = None
                else:
                    nmea_timestamp = nmea_time.timestamp()
                output_elements_columns = [
                    "sensor_id",
                    "valid_flag",
                    "last_updated_timestamp",
                    "latitude",
                    "longitude",
                    "gpgsa_pdop",
                    "north_south_indicator",
                    "east_west_indicator",
                    "altitude",
                    "ground_speed_km",
                    "degree_of_direction",
                    "number_of_satellites_used"
                ]
                output_elements = [
                        sensor_id,
                        valid_flag,
                        nmea_timestamp,
                        lat,
                        longi,
                        gpgsa_pdop,
                        nmea_ns_indicator, 
                        nmea_ew_indicator,
                        nmea_altitude,
                        gprmc_ground_speed,
                        gprmc_course_over_ground,
                        nmea_satellites_used
                ]
                # If there's GPS data csv files that's older than 7 days, remove them.
                file_list = [x for x in os.listdir(output_directory) if '.csv' in x]
                for each_file in file_list:
                    each_file_date = re.findall(r"[0-9]{1,5}_gps_([0-9-]+).csv", each_file)
                    if each_file_date == []:
                        continue
                    each_generated_data = datetime.datetime.strptime(each_file_date[0], "%Y-%m-%d")
                    if each_generated_data <= datetime.datetime(year=nmea_time.year, month=nmea_time.month, day=nmea_time.day) - datetime.timedelta(days=keep_days):
                        os.remove(f"{output_directory}/{each_file}")

                # If nmea_time is not None and there's no GPS data file, generate a GPS data csv file for that day.
                if nmea_time is not None:
                    file_full_path = f"{output_directory}/{sensor_id}_gps_{datetime.datetime.strftime(nmea_time, '%Y-%m-%d')}.csv"
                    if not os.path.isfile(file_full_path):
                        with open(file_full_path, "w") as f:
                            wr = csv.writer(f)
                            wr.writerow(output_elements_columns)
                    # Append each GPS data record into the file.
                    with open(file_full_path, "a") as f:
                        wr = csv.writer(f)
                        wr.writerow(output_elements)

                current_time2 = time.time() # Every send_interval seconds
                if send_to_cloud == True and current_time2 >= before_time2 + send_interval:
                    before_time2 = current_time2
                    # Send gps information to the Realtime server using an API if there's a ESP8266 sensor attached.
                    gps_auth = HTTPBasicAuth('admin', 'GgP$s_@dmin!5fgp3h84')
                    java_config_exist_flag = os.path.isfile(java_sensor_configuration_dir)
                    if java_config_exist_flag == False:
                        print("There's no java-sensor-configuration.yaml under /opt/lba_sensor/jar/")
                        print("Skipping sending GPS data via API call..")
                    else:
                        if sensor_id == 0:
                            print("SensorId is empty in java-sensor-configuration.yaml")
                            print("Skipping sending GPS data via API call..")
                        else:
                            sensor_id = sensor_id_str[0]
                            # If GPS data is invalid, don't send GPS data to the server
                            if nmea_position_fix == 0 or gprmc_status != "A":
                                print("GPS data is invalid. Skipping sending GPS data via API call..")
                            else:
                                body = {
                                    "sensorId": sensor_id,
                                    "datetime": nmea_timestamp,
                                    "latitude": lat,
                                    "longitude": longi,
                                    "gpgsaPdop": gpgsa_pdop,
                                    "nsIndicator": nmea_ns_indicator,
                                    "ewIndicator": nmea_ew_indicator,
                                    "positionFix": nmea_position_fix,
                                    "satellitesUsed": nmea_satellites_used,
                                    "altitude": nmea_altitude,
                                    "unit": nmea_units,
                                    "groundSpeed": gprmc_ground_speed,
                                    "courseOverGround": gprmc_course_over_ground,
                                    "gpggaChecksum": nmea_checksum,
                                    "gprmcStatus": gprmc_status,
                                    "gprmcChecksum": gprmc_checksum
                                }
                                res = requests.post(f"{server_url}/lba_gps/sensor-location", json=body, auth=gps_auth, verify=False)
                                if res.status_code == 200:
                                    print(f"GPS data has been succesfully sent to {server_url}.")
                                else:
                                    print(f"Sending GPS data has failed. Status Code: {res.status_code}")
        except Exception as e:
            print(f"Error occured in the loop: {e}")
        sleep(0.05)
except KeyboardInterrupt:
    sys.exit(0)
