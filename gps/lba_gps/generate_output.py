import sys
import time
import re
import os
import pandas as pd
import numpy as np
import datetime
import math
import json
pd.set_option('mode.chained_assignment', None)
import requests
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

if len(sys.argv) > 1:
    input_parameter = sys.argv[1]
else:
    input_parameter = None

java_sensor_configuration_dir = "/opt/lba_sensor/jar/java-sensor-configuration.yaml"
gps_configuration_dir = '/opt/lba_gps/lba_gps_configuration.json'
analytics_path = "/opt/lba_sensor/jar"
gps_data_path = "/opt/lba_gps"
resolution_list = [1, 10, 100, 1000, 10000]
round_dict = {1: 5, 10: 4, 100: 3, 1000: 2, 10000: 1, 100000: 0}

with open(gps_configuration_dir) as f:
    config_json = json.load(f)

# Load csv files for gps data and 1min-analytics
def load_csv_files(java_sensor_configuration_dir=java_sensor_configuration_dir, analytics_path=analytics_path, gps_data_path=gps_data_path, analytics_gps_timestamp_tup=(0,0)):
    with open(java_sensor_configuration_dir, "r") as f:
        java_sensor_config = f.readlines()
    config_sensor_line = [x for x in java_sensor_config if 'SensorId' in x][0]
    sensor_id_str = re.findall("[0-9]{3,5}", config_sensor_line)
    if sensor_id_str == []:
        raise Exception(f"Cannot read SensorId from {java_sensor_configuration_dir}")
    else:
        sensor_id = int(sensor_id_str[0])
    datetime_utc_now = datetime.datetime.utcnow() # orig
    input_utc_now_str = datetime.datetime.strftime(datetime_utc_now, "%Y-%m-%d")
    # input_utc_now_str = "2022-01-27" # test
    raw_min_analytics_df = pd.read_csv(f"{analytics_path}/{sensor_id}_1min-analytics_{input_utc_now_str}.csv", header=None)
    raw_min_analytics_df = raw_min_analytics_df.rename(columns={0:'sensor_id', 2:'datetime_utc', 5:'visitors'})
    raw_min_analytics_df = (raw_min_analytics_df[(raw_min_analytics_df['datetime_utc']>analytics_gps_timestamp_tup[0])
    &(raw_min_analytics_df['visitors']>0)].reset_index(drop=True)) # test. visitor filter added
    raw_gps_records_df = pd.read_csv(f"{gps_data_path}/{sensor_id}_gps_{input_utc_now_str}.csv")
    raw_gps_records_df = raw_gps_records_df[raw_gps_records_df['last_updated_timestamp']>analytics_gps_timestamp_tup[1]].reset_index(drop=True)
    # Read last timestamp
    if len(raw_min_analytics_df) <= 1 or len(raw_gps_records_df) <= 1:
        min_timestamp_val = min(analytics_gps_timestamp_tup[0], analytics_gps_timestamp_tup[1])
        analytics_last_timestamp = min_timestamp_val
        gps_last_timestamp = min_timestamp_val
    else:
        analytics_last_timestamp = raw_min_analytics_df['datetime_utc'].tail(1).values[0]
        gps_last_timestamp = raw_gps_records_df['last_updated_timestamp'].tail(1).values[0]

    print(f"length of raw_min_analytics: {len(raw_min_analytics_df)}") # test
    print(f"length of raw_gps_records_df: {len(raw_gps_records_df)}") # test
    return raw_min_analytics_df, raw_gps_records_df, (analytics_last_timestamp, gps_last_timestamp)

def pdcut_time_bins_32bit(df, bins_df, right=False, precision=5, chunk=100):
    if len(bins_df) <= chunk:
        # return pd.cut(df, bins=bins_df, right=right, precision=precision).apply(lambda x: x.left).astype('datetime64[ns]') # orig
        return pd.cut(df, bins=bins_df, right=right, precision=precision).apply(lambda x: x.right).astype('datetime64[ns]') # test
    else:
        for i in range((len(bins_df)//chunk)+1):
            if i == 0:
                # output_df = pd.cut(df, bins=bins_df.iloc[:(i+1)*chunk], right=right, precision=precision).apply(lambda x: x.left).astype('datetime64[ns]') # orig
                output_df = pd.cut(df, bins=bins_df.iloc[:(i+1)*chunk], right=right, precision=precision).apply(lambda x: x.right).astype('datetime64[ns]') # test
            else:
                split_cut_df = pd.cut(df, bins=bins_df.iloc[i*chunk:(i+1)*chunk], right=right, precision=precision)
                if len(split_cut_df.dropna()) == 0:
                    # split_cut_df = split_cut_df.apply(lambda x: x.left).astype('datetime64[ns]') # orig
                    split_cut_df = split_cut_df.apply(lambda x: x.right).astype('datetime64[ns]') # test
                    continue
                else:
                    # output_df = pd.concat([output_df, split_cut_df.apply(lambda x: x.left).astype('datetime64[ns]')], axis=1) # orig
                    output_df = pd.concat([output_df, split_cut_df.apply(lambda x: x.right).astype('datetime64[ns]')], axis=1) # test
        if len(output_df.shape) == 2:
            output_df = output_df.max(axis=1)
        return output_df
        
def generate_gps_records_df(raw_min_analytics_df, raw_gps_records_df, _32bit=False):
    # If there isn't new data, skip this cycle and use the same last-timestamp.
    if len(raw_min_analytics_df) <= 1 or len(raw_gps_records_df) <= 1:
        print(f"Not enough data has been gathered from raw records.")
        # print(f"raw_min_analytics_df: {len(raw_min_analytics_df)}, raw_gps_records_df: {len(raw_gps_records_df)}")
        return pd.DataFrame()
    raw_min_analytics_df = raw_min_analytics_df.dropna(axis=1)
    raw_min_analytics_df.loc[:, 'datetime_utc'] = pd.to_datetime(raw_min_analytics_df['datetime_utc'], unit='s')
    raw_min_analytics_df = raw_min_analytics_df.rename(columns={0:'sensor_id', 2:'datetime_utc', 5:'visitors'})
    raw_min_analytics_df = raw_min_analytics_df.drop_duplicates(subset='datetime_utc').reset_index(drop=True)
    raw_gps_records_df = raw_gps_records_df[raw_gps_records_df['valid_flag']==True][['sensor_id','latitude','longitude','last_updated_timestamp']].reset_index(drop=True)
    raw_gps_records_df.loc[:, 'last_updated_timestamp'] = pd.to_datetime(raw_gps_records_df['last_updated_timestamp'], unit='s')
    if _32bit == False:
        analytics_bins_series = pd.cut(raw_gps_records_df['last_updated_timestamp'], bins=raw_min_analytics_df['datetime_utc'], right=False, precision=5).apply(lambda x: x.left).astype('datetime64[ns]')
    else:
        analytics_bins_series = pdcut_time_bins_32bit(raw_gps_records_df['last_updated_timestamp'], raw_min_analytics_df['datetime_utc'], right=False, precision=5) # orig

    # analytics_bins_series.name = 'bins_left' # orig
    # raw_gps_records_df = pd.concat([raw_gps_records_df, analytics_bins_series], axis=1) # orig
    # raw_gps_records_df.loc[:, 'bins_left'] = raw_gps_records_df['bins_left'].astype('datetime64[ns]') # orig

    analytics_bins_series.name = 'bins_right' # test
    raw_gps_records_df = pd.concat([raw_gps_records_df, analytics_bins_series], axis=1) # test
    raw_gps_records_df.loc[:, 'bins_right'] = raw_gps_records_df['bins_right'].astype('datetime64[ns]') # test
 
    # raw_gps_records_df = raw_gps_records_df.merge(raw_min_analytics_df[['datetime_utc','visitors']], how='left', left_on='bins_left', right_on='datetime_utc').drop('bins_left', axis=1) # orig
    raw_gps_records_df = raw_gps_records_df.merge(raw_min_analytics_df[['datetime_utc','visitors']], how='left', left_on='bins_right', right_on='datetime_utc').drop('bins_right', axis=1) # test

    processed_gps_records_df = raw_gps_records_df.rename(columns={'latitude':'gps_latitude', 'longitude':'gps_longitude', 'last_updated_timestamp':'gps_datetime', 'datetime_utc':'analytics_datetime'})
    return processed_gps_records_df

def generate_output(processed_gps_df, resolution_list=resolution_list, round_dict=round_dict):
    if len(processed_gps_df) == 0:
        print(f"processed_gps_df is Empty.")
        return None
    # Generate raw records that combined the GPS data and 1min analytics
    output_df = processed_gps_df[['sensor_id','gps_datetime','analytics_datetime','visitors','gps_latitude','gps_longitude']]
    for each_res in resolution_list:
        allocated_coords_df = output_df[['gps_latitude', 'gps_longitude']].applymap(lambda x: round(x,round_dict[each_res]))
        allocated_coords_df = allocated_coords_df.rename(columns={'gps_latitude': f'latitude_res_{each_res}', 'gps_longitude': f'longitude_res_{each_res}'})
        output_df = pd.concat([output_df, allocated_coords_df], axis=1)
    output_df = output_df.dropna(subset=['analytics_datetime'])
    if len(output_df) == 0:
        print("output_df is Empty after dropping NaN value of analytics_datetime.")
        return
    # Generate True False table based on raw combined records
    unique_check_df = pd.DataFrame()
    grouped_output_df = output_df.groupby('analytics_datetime')
    for each_res in resolution_list:
        unique_check_lat_series = grouped_output_df.apply(lambda x: len(x[f'latitude_res_{each_res}'].unique())).apply(lambda x: True if x == 1 else False)
        unique_check_longi_series = grouped_output_df.apply(lambda x: len(x[f'longitude_res_{each_res}'].unique())).apply(lambda x: True if x == 1 else False)
        unique_check_coord_series = unique_check_lat_series & unique_check_longi_series
        unique_check_coord_series.name = f"coords_res_{each_res}"
        unique_check_df = pd.concat([unique_check_df, unique_check_coord_series], axis=1)

    # Generate final output dataframe
    total_true_coords_df = pd.DataFrame()
    for each_res in resolution_list:
        true_index_list = unique_check_df[unique_check_df[f"coords_res_{each_res}"]==True].index
        avg_visitor_series = output_df[output_df['analytics_datetime'].isin(true_index_list)].groupby([f'latitude_res_{each_res}',f'longitude_res_{each_res}'])['visitors'].mean()
        avg_visitor_series.name = 'avg_visitor'
        detected_gps_total_series = output_df[output_df['analytics_datetime'].isin(true_index_list)].groupby([f'latitude_res_{each_res}',f'longitude_res_{each_res}'])['visitors'].count()
        detected_gps_total_series.name= 'detected_gps_total'

        if len(avg_visitor_series) != 0:
            coords_true_df = pd.concat([avg_visitor_series, detected_gps_total_series], axis=1)
            coords_true_df = coords_true_df.reset_index().rename(
                columns={f"latitude_res_{each_res}":"assigned_latitude", 
                f"longitude_res_{each_res}":"assigned_longitude"}
                )
            coords_true_df.loc[:, 'avg_visitor'] = coords_true_df.loc[:, 'avg_visitor'].round()
            coords_true_df['resolution'] = each_res
            total_true_coords_df = pd.concat([total_true_coords_df, coords_true_df], axis=0)
    total_true_coords_df['timestamp'] = int(time.time())
    total_true_coords_df = total_true_coords_df[['timestamp']+[x for x in total_true_coords_df.columns if x != 'timestamp']]

    return total_true_coords_df

def remove_old_matrix(gps_data_path=gps_data_path, config_json=config_json):
    utc_datetime_now = datetime.datetime.utcnow()
    utc_datetime_date = datetime.datetime(year=utc_datetime_now.year, month=utc_datetime_now.month, day=utc_datetime_now.day)
    file_list = [x for x in os.listdir(gps_data_path) if '.csv' in x]
    # If there's GPS visitor output csv files that's older than n days, remove them.
    for each_file in file_list:
        each_file_date = re.findall(r"[0-9]{1,5}_gps_visitor_output_([0-9-]+).csv", each_file)
        if each_file_date == []:
            continue
        each_generated_data = datetime.datetime.strptime(each_file_date[0], "%Y-%m-%d")
        if each_generated_data <= utc_datetime_date - datetime.timedelta(days=config_json['keep_days']):
            os.remove(f"{gps_data_path}/{each_file}")

# Store and send the generated output if needed
def store_send_output(valid_output_df, multiplier=100000, java_sensor_configuration_dir=java_sensor_configuration_dir, gps_data_path=gps_data_path, config_json=config_json):
    utc_datetime_now = datetime.datetime.utcnow()
    utc_datetime_date = datetime.datetime(year=utc_datetime_now.year, month=utc_datetime_now.month, day=utc_datetime_now.day)
    utc_datetime_date_str = datetime.datetime.strftime(utc_datetime_date, "%Y-%m-%d")

    # Fetch sensor id from /opt/lba_sensor/jar/java-sensor-configuration.yaml
    with open(java_sensor_configuration_dir, "r") as f:
        java_sensor_config = f.readlines()
    config_sensor_line = [x for x in java_sensor_config if 'SensorId' in x][0]
    sensor_id_str = re.findall("[0-9]{3,5}", config_sensor_line)
    if sensor_id_str == []:
        sensor_id = 0
    else:
        sensor_id = int(sensor_id_str[0])

    # Process valid_output_df to contain sensor_id
    processed_valid_output_df = valid_output_df
    processed_valid_output_df.loc[:, ['assigned_latitude','assigned_longitude']] = (processed_valid_output_df.loc[:, ['assigned_latitude','assigned_longitude']] * multiplier).astype(int)
    processed_valid_output_df.loc[:, 'avg_visitor'] = processed_valid_output_df.loc[:, 'avg_visitor'].astype(int)
    processed_valid_output_df['sensor_id'] = sensor_id
    processed_valid_output_df = (
        processed_valid_output_df
        [['sensor_id']+[x for x in processed_valid_output_df.columns if x not in ["sensor_id","avg_visitor","detected_gps_total"]]
        + ['avg_visitor','detected_gps_total']]
        )

    file_full_path = f"{gps_data_path}/{sensor_id}_gps_visitor_output_{utc_datetime_date_str}.csv"
    # If the file doesn't exist, create a new file.
    if not os.path.isfile(file_full_path):
        processed_valid_output_df.to_csv(file_full_path, index=False, header=False)
        print(f"processed_valid_output_df:{processed_valid_output_df.shape} have been saved as a new csv file.") # test
    # If the file already exists, append valid_output into the existing file.
    else:
        processed_valid_output_df.to_csv(file_full_path, mode='a', index=False, header=False)
        print(f"processed_valid_output_df:{processed_valid_output_df.shape} have been appended to the existing csv file.") # test

    if config_json['send_output_to_cloud'] == True and sensor_id != 0:
    
        server_url = config_json['map_data_server']
        basic_auth = HTTPBasicAuth('admin', 'dAt@sd0On&_@dmin!')
        files = {
            "file": open(f"{gps_data_path}/{sensor_id}_gps_visitor_output_{utc_datetime_date_str}.csv", 'rb')
        }
        res = requests.post(f"{server_url}/lba_dataspoon/map/upload", files=files, auth=basic_auth, verify=False)
        if res.status_code == 200:
            print(f"visitor output data has been succesfully sent to {server_url}.")
        else:
            print(f"Sending visitor output data has failed. Status Code: {res.status_code}")

    return processed_valid_output_df



if input_parameter != 'instant':
    time.sleep(config_json['output_interval'])
while True:
    remove_old_matrix(gps_data_path=gps_data_path, config_json=config_json)
    raw_min_analytics_df, raw_gps_records_df, (analytics_last_timestamp, gps_last_timestamp) = load_csv_files()
    processed_gps_df = generate_gps_records_df(raw_min_analytics_df, raw_gps_records_df, _32bit=True)
    valid_output_df = generate_output(processed_gps_df)
    _ = store_send_output(valid_output_df)
    time.sleep(config_json['output_interval'])