#!/bin/bash

SCRIPT_DIR=$(dirname "$0")

APP_DIR='/opt/lba_sensor_setup'
APP_BUNDLES=($SCRIPT_DIR/app-bundles/*)
NUM_OF_BUNDLES=${#APP_BUNDLES[@]}
REGEX_DIGIT='^[0-9]+$'

if [ $NUM_OF_BUNDLES == 1 ]
then
    i=0
    tar -xzf ${APP_BUNDLES[$i]} -C $SCRIPT_DIR
    cp -a $SCRIPT_DIR/package/. $APP_DIR
    # sudo rsync -av -P $SCRIPT_DIR/package $APP_DIR
else
    while true; do
        echo "--- App bundles"
        i=0
        for bundle in "${APP_BUNDLES[@]}"; do
            i=$(( i + 1 ))
            echo "$i - $bundle"
        done
        read -p 'Select app bundle to install: ' selected
        if [[ $selected =~ $REGEX_DIGIT ]] ; then
            if [ "$selected" -gt 0 ] && [ "$selected" -le $NUM_OF_BUNDLES ]; then
                index=$(( selected - 1))
                tar -xzf ${APP_BUNDLES[$index]} -C $SCRIPT_DIR
                cp -a $SCRIPT_DIR/package/. $APP_DIR
                # sudo rsync -av -P $SCRIPT_DIR/package $APP_DIR
                break 
            fi
        fi
    
        echo "INVALID INPUT\n"
    done
fi

echo "--- Restarting lba_sensor_setup"
pm2 startOrRestart $APP_DIR/ecosystem.config.js --env production

echo "--- Cleaning up"
rm -rf $SCRIPT_DIR/package
