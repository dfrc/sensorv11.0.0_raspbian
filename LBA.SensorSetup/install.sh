#!/bin/bash

# wget https://nodejs.org/dist/v15.12.0/node-v15.12.0.tar.gz
# wget http://npmjs.org/install.sh
# npm-bundle pm2

SCRIPT_DIR=$(dirname "$0")

APP_DIR='/opt/lba_sensor_setup'
APP_BUNDLES=($SCRIPT_DIR/app-bundles/*)
NUM_OF_BUNDLES=${#APP_BUNDLES[@]}
REGEX_DIGIT='^[0-9]+$'

echo "--- Removing crontab temporarily"
crontab -r

echo "--- Installing NodeJS"
sudo tar -C /usr/local --strip-components 1 -xzf $SCRIPT_DIR/node-v15.12.0-linux-arm64.tar.gz

echo "--- Installing PM2 package"
npm i -g $SCRIPT_DIR/pm2-4.5.5.tgz --prefer-offline

sudo mkdir -p $APP_DIR
sudo chown -R pi $APP_DIR
sudo chgrp -R pi $APP_DIR

if [ $NUM_OF_BUNDLES == 1 ]
then
    tar -xzf ${APP_BUNDLES[0]} -C $SCRIPT_DIR
    cp -a $SCRIPT_DIR/package/. $APP_DIR
    # sudo rsync -av -P $SCRIPT_DIR/package $APP_DIR
else
    while true; do
        echo "--- App bundles"
        i=0
        for bundle in "${APP_BUNDLES[@]}"; do
            i=$(( i + 1 ))
            echo "$i - $bundle"
        done
        read -p 'Select app bundle to install: ' selected
        if [[ $selected =~ $REGEX_DIGIT ]] ; then
            if [ "$selected" -gt 0 ] && [ "$selected" -le $NUM_OF_BUNDLES ]; then
                index=$(( selected - 1))
                tar -xzf ${APP_BUNDLES[$index]} -C $SCRIPT_DIR
                cp -a $SCRIPT_DIR/package/. $APP_DIR
                # sudo rsync -av -P $SCRIPT_DIR/package $APP_DIR
                break 
            fi
        fi
    
        echo "INVALID INPUT\n"
    done
fi

sudo ufw allow 10888
echo "--- Opened port 10888"

echo "--- Starting lba_sensor_setup"
pm2 startOrRestart $APP_DIR/ecosystem.config.js --env production
# pm2 startup
echo $(pm2 startup | tail -n1) | bash
pm2 save

echo "--- Starting crontab again"
bash /opt/utils/Crontab.sh

echo "--- Cleaning up"
rm -rf $SCRIPT_DIR/package
