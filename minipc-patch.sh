#!/bin/bash
echo "MiniPC Patch. 2020-07-13"

echo "-- sudo command without password for user($USER) patch --"
echo "$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers

echo "-- remote_control_command x86_64 install --"
sudo cp minipc-patch/remote_control_command /opt/lba_remote/

echo "-- captive portal config --"
sudo wifi-ap.config set share.network-interface=enp1s0
sudo wifi-ap.config set wifi.interface=wlp3s0
sudo cp minipc-patch/nodogsplash.conf /etc/nodogsplash/

echo "Patch OK"
echo "Reboot required"
