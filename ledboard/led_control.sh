#!/bin/sh

#
# Internet Connection Check
#
# active: true / false
# domain / ip address
# port number (default: 80)
#
internet_connection_check_active=true
internet_connection_check_address="google.com"
internet_connection_check_port=80

#
# Server Connection Check
#
# active: true / false
# domain / ip address
# port number (default: 80)
#
server_connection_check_active=true
server_connection_check_address="master.lbasense.com"
server_connection_check_port=80

#
# LED Type
#
# 3 = 3-color 1 LED
# 4 = 4 LED Board
#
led_type=3

#
# LED Priority (3-color 1 LED)
# Server > Internet > Power
# Server   : Green
# Internet : Blue
# Power    : Red
#

#####################
#
# LED CONTROL PART
#
# Another methods for check.
#  ping -c 1 -W 3 $internet_connection_check_address
#  wget -q --spider $internet_connection_check_address
#  curl -Is $internet_connection_check_address
#  echo > /dev/tcp/$internet_connection_check_address/$internet_connection_check_port
#  * nc -zw1 $internet_connection_check_address $internet_connection_check_port
#  nmap $internet_connection_check_address -p $internet_connection_check_port
#


#
# Internet LED
#
if $internet_connection_check_active
then
  nc -zw1 $internet_connection_check_address $internet_connection_check_port
  ret=$?
  if [ $ret -eq 0 ]
  then
    # internet connected
    d2=true
  else
    # internet disconnected
    d2=false
  fi
else
  # internet connection check disabled
  d2=true
fi



#
# Server LED
#
if $server_connection_check_active
then
  nc -zw1 $server_connection_check_address $server_connection_check_port
  ret=$?
  if [ $ret -eq 0 ]
  then
    # server connected
    d1=true
  else
    # server disconnected
    d1=false
  fi
else
  # server connection check disabled
  d1=true
fi



#
# Power LED
#
# * temporaril disabled. (kernel panic related)
#
#if [ "$(cat /sys/devices/platform/soc/soc:firmware/get_throttled)" = "0" ]
#then
  # power condition is ok
  d4=true
#else
  # under-voltage detected or throttled
#  d4=false
#fi


#
# LED Control
#
if [ $led_type -eq 3 ]
then
  if [ "$d1" = true ]
  then
    echo "default-on" | sudo tee /sys/class/leds/lba_led_d1/trigger
    echo "none" | sudo tee /sys/class/leds/lba_led_d2/trigger
  else
    echo "none" | sudo tee /sys/class/leds/lba_led_d1/trigger
    if [ "$d2" = true ]
    then
      echo "default-on" | sudo tee /sys/class/leds/lba_led_d2/trigger
    else
      echo "none" | sudo tee /sys/class/leds/lba_led_d2/trigger
    fi
  fi

  if [ "$d1" = false ] && [ "$d2" = false ]
  then
    echo "default-on" | sudo tee /sys/class/leds/lba_led_d4/trigger
  else
    echo "none" | sudo tee /sys/class/leds/lba_led_d4/trigger
  fi

  if [ "$d4" = false ]
  then
    echo "timer" | sudo tee /sys/class/leds/lba_led_d4/trigger
  fi
else
  if [ "$d4" = true ]
  then
    echo "default-on" | sudo tee /sys/class/leds/lba_led_d4/trigger
  else
    echo "timer" | sudo tee /sys/class/leds/lba_led_d4/trigger
  fi

  if [ "$d2" = true ]
  then
    echo "default-on" | sudo tee /sys/class/leds/lba_led_d2/trigger
  else
    echo "none" | sudo tee /sys/class/leds/lba_led_d2/trigger
  fi

  if [ "$d1" = true ]
  then
    echo "default-on" | sudo tee /sys/class/leds/lba_led_d1/trigger
  else
    echo "none" | sudo tee /sys/class/leds/lba_led_d1/trigger
  fi
fi

#
#
#####################
