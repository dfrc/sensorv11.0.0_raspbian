#!/bin/sh
YELLOW='\033[1;33m'
RED='\033[0;31m'
BLUE='\033[1;34m'
SET='\033[0m'

echo "${YELLOW}--Billboard installation on ubuntu system--${SET}"
sudo mkdir /opt/billboard
sudo cp -r billboard /opt/
sudo ln -s /opt/billboard/jar/edge-billboard-1.4.2.jar /opt/billboard/billboard-latest
ls -l /opt/billboard/

echo "${YELLOW}--Create service--${SET}"
sudo cp billboard/billboard.service /etc/systemd/system



sudo systemctl daemon-reload
sudo systemctl enable billboard.service
sudo systemctl start billboard

