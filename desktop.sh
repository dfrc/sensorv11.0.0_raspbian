set -e
sudo apt-get update -y
./network-manager-gui-patch.sh
sudo apt-get install ubuntu-desktop -y
sudo apt-get install openjfx -y
sudo apt-get install chromium-browser --yes
./change_resolution.sh
sudo reboot
