#!/bin/bash
set -e
# color-echo.sh: Вывод цветных сообщений.

black='\E[30;1m'
red='\E[31;1m'
green='\E[32;1m'
yellow='\E[33;1m'
blue='\E[34;1m'
magenta='\E[35;1m'
cyan='\E[36;1m'
white='\E[37;1m'

cecho ()                     # Color-echo.
                             # Аргумент $1 = текст сообщения
                             # Аргумент $2 = цвет
{
local default_msg="Нет сообщений." # Не обязательно должна быть локальной.

message=${1:-$default_msg}   # Текст сообщения по-умолчанию.
color=${2:-$white}           # Цвет по-умолчанию белый.

  echo -e "$color"
  echo "$message"
  tput sgr0                  # Восстановление первоначальных настроек терминала.
  return
}


# ----------------------------------------------------
#cecho "Синий текст..." $blue
#cecho "Пурпурный текст." $magenta
#cecho "Позеленевший от зависти." $green
#cecho "Похоже на красный?" $red
#cecho "Циан, более известный как цвет морской волны." $cyan
#cecho "Цвет не задан (по-умолчанию черный)."
# ----------------------------------------------------

echo "BLE Sensor Installation 2020-09-22"
if [[ $USER == root ]]; then
  cecho "This script must be run not root" $red 1>&2
  exit 1
fi

echo $USER
sudo pwd



cecho "--Install Dependencies--" $yellow
cecho "--from APT--" $yellow
sudo apt install build-essential git libmicrohttpd-dev pkg-config zlib1g-dev libnl-3-dev libnl-genl-3-dev libcap-dev libpcap-dev libnm-dev libdw-dev libsqlite3-dev libprotobuf-dev libprotobuf-c-dev protobuf-compiler protobuf-c-compiler libsensors4-dev libusb-1.0-0-dev python3 python3-setuptools python3-protobuf python3-requests python3-numpy python3-serial python3-usb python3-dev librtlsdr0 libubertooth-dev libbtbb-dev mono-runtime libwebsockets-dev
cecho "--DONE--" $blue



cecho "--Install kismet-main--" $yellow
git clone https://www.kismetwireless.net/git/kismet.git
cd kismet
./configure
make
sudo make suidinstall
sudo usermod -aG kismet $USER
cd ..
cecho "--DONE--" $blue



cecho "--Install LBA BLE Sensor 20-09-22--" $yellow

sudo cp -rf root/* /

cecho "--Configure LBA BLE Sensor--" $yellow
sudo systemctl enable lba-ble.sensor.service

cecho "--Need Reboot--" $yellow
cecho "--DONE--" $blue
