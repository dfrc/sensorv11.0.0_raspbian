#!/bin/bash

echo "Addon Captive Portal - HTTP - getMAC"
echo ""
echo " - Copying files..."

sudo cp -rf LBA.CP.HTTP_getMAC/* /

echo "  :Done"

echo ""
echo " - Addon install completed."
echo "  :No reboot required."
echo ""
echo " -> http://10.0.60.1/getMAC"
echo " <- [client MAC address, ex) 12:34:56:ab:cd:ef]"
