!/bin/bash
cat << EOF
-----------------------------------------------------------
Disable Captive Portal
-----------------------------------------------------------
EOF

while true; do
  read -p "Disable Captive Portal? (y/n) " yn
  case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer y or n.";;
  esac
done

sudo cp -rf root/var/www/html/* /var/www/html/

cat << EOF
---------------------------------
Done.
---------------------------------
EOF
