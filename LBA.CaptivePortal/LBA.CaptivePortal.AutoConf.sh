#!/bin/bash
set -e
# color-echo.sh: Вывод цветных сообщений.

black='\E[30;1m'
red='\E[31;1m'
green='\E[32;1m'
yellow='\E[33;1m'
blue='\E[34;1m'
magenta='\E[35;1m'
cyan='\E[36;1m'
white='\E[37;1m'

cecho ()                     # Color-echo.
                             # Аргумент $1 = текст сообщения
                             # Аргумент $2 = цвет
{
local default_msg="Нет сообщений." # Не обязательно должна быть локальной.

message=${1:-$default_msg}   # Текст сообщения по-умолчанию.
color=${2:-$white}           # Цвет по-умолчанию белый.

  echo -e "$color"
  echo "$message"
  tput sgr0                  # Восстановление первоначальных настроек терминала.
  return
}


# ----------------------------------------------------
#cecho "Синий текст..." $blue
#cecho "Пурпурный текст." $magenta
#cecho "Позеленевший от зависти." $green
#cecho "Похоже на красный?" $red
#cecho "Циан, более известный как цвет морской волны." $cyan
#cecho "Цвет не задан (по-умолчанию черный)."
# ----------------------------------------------------

echo "Captive Portal v1.0.2.1 Installation 2021-01-28"
echo "Installation script had been modified for Ubuntu Desktop 20.0.10 on 15-Nov-2020"
if [[ $USER == root ]]; then
  cecho "This script must be run not root" $red 1>&2
  exit 1
fi

echo $USER
sudo pwd

echo "Step 0/14" 
sudo apt install curl


cecho "--Repository: Add Source and Update--" $yellow

# add-apt-repository may not be present on some Ubuntu releases:
# sudo apt-get install python-software-properties
echo "Step 1/14" 
sudo add-apt-repository -y -r ppa:chris-lea/node.js
sudo rm -f /etc/apt/sources.list.d/chris-lea-node_js-*.list
sudo rm -f /etc/apt/sources.list.d/chris-lea-node_js-*.list.save

echo "Step 2/14" 
curl -sSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
# wget can also be used:
# wget --quiet -O - https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -

# Replace with the branch of Node.js or io.js you want to install: node_6.x, node_8.x, etc...
VERSION=node_14.x
# The below command will set this correctly, but if lsb_release isn't available, you can set it manually:
# - For Debian distributions: jessie, sid, etc...
# - For Ubuntu distributions: xenial, bionic, etc...
# - For Debian or Ubuntu derived distributions your best option is to use the codename corresponding to the upstream release your distribution is based off. This is an advanced scenario and unsupported if your distribution is not listed as supported per earlier in this README.
DISTRO="$(lsb_release -s -c)"
echo "deb https://deb.nodesource.com/$VERSION $DISTRO main" | sudo tee /etc/apt/sources.list.d/nodesource.list
echo "deb-src https://deb.nodesource.com/$VERSION $DISTRO main" | sudo tee -a /etc/apt/sources.list.d/nodesource.list

sudo apt update
cecho "--DONE--" $blue

cecho "--APT Upgrade--" $yellow
sudo apt upgrade -y
cecho "--DONE--" $blue


echo "Step 3/14" 
cecho "--disable systemd-resolved service--" $yellow
sudo systemctl stop systemd-resolved.service
sudo systemctl disable systemd-resolved.service
sudo rm -v /etc/resolv.conf
sudo touch /etc/resolv.conf
echo "nameserver 1.1.1.1" | sudo tee /etc/resolv.conf
cecho "--DONE--" $blue



echo "Step 4/14" 
cecho "--Install Dependencies--" $yellow
cecho "--from APT--" $yellow
sudo DEBIAN_FRONTEND=noninteractive apt install -y build-essential zzuf socat gnutls-bin libgcrypt-dev libcurl4-gnutls-dev libgnutls28-dev pkg-config net-tools wireless-tools iw uncrustify libtool autopoint texinfo nodejs apache2 php php-bcmath libapache2-mod-php rfkill netfilter-persistent iptables-persistent
cecho "--from NPM--" $yellow
sudo npm install -g socket.io@2.2.0
cecho "--DONE--" $blue



echo "Step 5/14" 
cecho "--Install libmicrohttpd v0.9.70--" $yellow
wget https://ftp.gnu.org/gnu/libmicrohttpd/libmicrohttpd-0.9.70.tar.gz
tar  -xf libmicrohttpd-0.9.70.tar.gz
cd libmicrohttpd-0.9.70
./configure
make -j$(nproc)
sudo make install
sudo ldconfig
cd ..
cecho "--DONE--" $blue



echo "Step 6/14" 
cecho "--Install openNDS v5.2.0--" $yellow
wget https://codeload.github.com/opennds/opennds/tar.gz/v5.2.0
tar -xf v5.2.0
cd openNDS-5.2.0
make -j$(nproc)
sudo make install
sudo systemctl enable opennds
cd ..
cecho "--DONE--" $blue



echo "Step 7/14" 
cecho "--Install LBA CaptivePortal v1.0.2.1 21-01-28--" $yellow

sudo apt install -y dnsmasq dhcpcd5 hostapd

sudo cp -rf root/* /

echo "Step 8/14" 
cecho "--Configure LBA CaptivePortal--" $yellow
sudo systemctl unmask hostapd.service
sudo systemctl enable hostapd.service
sudo systemctl enable lba-captiveportal-node.service
echo "Step 9/14" 
sudo a2enmod rewrite
echo "Step 10/14" 
sudo a2ensite lba.captiveportal.android.conf
echo "Step 11/14" 
sudo a2ensite opennds.fas.conf

echo "Step 12/14" 
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
echo "Step 13/14" 
sudo netfilter-persistent save

echo "Step 14/14" 
sudo ln /opt/lba-captiveportal/lcp_cli.php /bin/lcp_cli

while true; do
  read -p "Disable Captive Portal? (y/n) " yn
  case $yn in
    [Yy]* )
      sudo systemctl stop opennds.service
      sudo systemctl disable opennds.service
      break;;
    [Nn]* )
      break;;
    * ) echo "Please answer y or n.";;
  esac
done

cecho "--Need Reboot--" $yellow
cecho "--DONE--" $blue
