function requireGlobal(packageName) {
  var childProcess = require('child_process');
  var path = require('path');
  var fs = require('fs');

  var globalNodeModules = childProcess.execSync('npm root -g').toString().trim();
  var packageDir = path.join(globalNodeModules, packageName);
  if (!fs.existsSync(packageDir))
    packageDir = path.join(globalNodeModules, 'npm/node_modules', packageName); //find package required by old npm

  if (!fs.existsSync(packageDir))
    throw new Error('Cannot find global module \'' + packageName + '\'');

  var packageMeta = JSON.parse(fs.readFileSync(path.join(packageDir, 'package.json')).toString());
  var main = path.join(packageDir, packageMeta.main);

  return require(main);
}

const io = requireGlobal('socket.io')();
const { exec } = require('child_process');

io.on('connection', client => {
  console.log('a user connected : ' + client.handshake.address + ', ' + client.id);

  client.on('mac', data => {
    client.id = data.iam;
    console.log(client.id + ' user set mac : ' + client.handshake.address);
  });

  client.on('auth', function(data, ret) {
    console.log(client.id + ' user tyring auth : ' + client.handshake.address);
    exec('ndsctl auth ' + client.id, (error, stdout, stderr) => {
      console.log(stdout);
    });
    ret(true);
  });

  client.on('deauth', data => {
    console.log(client.id + ' user set deauth : ' + client.handshake.address);
    client.on('disconnect', () => {
      console.log(client.id + ' user disconnected : ' + client.handshake.address);
      exec('ndsctl deauth ' + client.id, (error, stdout, stderr) => {
        console.log(stdout);
      });
      exec('hostapd_cli deauthenticate ' + client.id, (error, stdout, stderr) => {
        console.log(stdout);
      });
    });
  });
});

io.listen(2230);

console.log('listening on *:2230');
