<?php
$key = "LBASense1234567890";

date_default_timezone_set("UTC");

if (isset($_SERVER['HTTPS'])) {
  $protocol = "https://";
} else {
  $protocol = "http://";
}

$invalid = "";
$cipher = "AES-256-CBC";
$docroot = $_SERVER['DOCUMENT_ROOT'];
$me = $_SERVER['SCRIPT_NAME'];
$home = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

$header = "LBASense Captive Portal";


if (isset($_GET['fas']) and isset($_GET['iv']))  {
  $string = $_GET['fas'];
  $iv = $_GET['iv'];
  $decrypted = openssl_decrypt(base64_decode($string), $cipher, $key, 0, $iv);
  $dec_r = explode(", ", $decrypted);

  foreach ($dec_r as $dec) {
    list($name, $value) = explode("=", $dec);
    if ($name == "clientip") { $clientip = $value; }
    if ($name == "clientmac") { $clientmac = $value; }
    if ($name == "gatewayname") { $gatewayname = $value; }
    if ($name == "tok") { $tok = $value; }
    if ($name == "gatewayaddress") { $gatewayaddress = $value; }
    if ($name == "authdir") { $authdir = $value; }
    if ($name == "originurl") { $originurl = $value; }
    if ($name == "clientif") { $clientif = $value; }
  }
  $client_zone_r = explode(" ", trim($clientif));

  if (!isset($client_zone_r[1])) {
    $client_zone = "LocalZone:".$client_zone_r[0];
  } else {
    $client_zone = "MeshZone:".str_replace(":", "", $client_zone_r[1]);
  }

} else if (isset($_GET["status"])) {
  $gatewayname = $_GET["gatewayname"];
  $gatewayaddress = $_GET["gatewayaddress"];
  $originurl = "";
  $loggedin = true;
} else {
  $invalid = true;
}

if (!isset($gatewayname)) {
  $gatewayname = "openNDS";
}

// https://stackoverflow.com/a/1273535
function bchexdec($hex) {
  $dec = 0;
  $len = strlen($hex);
  for ($i = 1; $i <= $len; $i++) {
    $dec = bcadd($dec, bcmul(strval(hexdec($hex[$i - 1])), bcpow('16', strval($len - $i))));
  }

  return $dec;
}

$conf = parse_ini_file("/opt/lba-captiveportal/lcp.conf", false, INI_SCANNER_TYPED);
$redirect_url = urldecode($conf["redirect_url"]);
$redirect_url = str_replace("{{clientmac}}", $clientmac, $redirect_url);
$redirect_url = str_replace("{{clientmaclong}}", bchexdec(preg_replace('/[^0-9A-Fa-f]/i', '', $clientmac)), $redirect_url);

$auto_disconnect = $conf["auto_disconnect"];

$loading_img = $conf["loading_img"];

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Cache-Control: no-cache");
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-realidate">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="0">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Loading...</title>
<script src="/js/jquery-3.5.1.min.js"></script>
<script src="/js/socket.io.js"></script>
<script>
const socket = io('10.0.60.1:2230', { query: "mac = <?php echo $clientmac; ?>" });
</script>
<style>
* { padding: 0; margin: 0; width: 100%; height: 100%; border: none; }
body {
  background: url("/img/<?php echo $loading_img; ?>") no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
</style>
</head>
<body>
<script>
socket.emit('mac', { iam: '<?php echo $clientmac; ?>' });
setTimeout(function() {
  socket.emit('auth', { iwant: 'you' }, function(yes) {
    <?php echo $auto_disconnect ? "socket.emit('deauth', { iwill: 'missyou' });" : ""; ?>
    $('body').html('<iframe src="<?php echo $redirect_url; ?>"></iframe>');
    setTimeout(function() {
      $('body').css('background-image', 'none');
    }, 1000);
  });
}, 1500);
</script>
</body>
</html>
