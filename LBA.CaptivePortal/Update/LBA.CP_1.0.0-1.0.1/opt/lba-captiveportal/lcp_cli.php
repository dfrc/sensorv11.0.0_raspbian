#!/bin/php
<?php
const VERSION = "1.0.1";

function help() {
  echo PHP_EOL;
  echo "LBA CaptivePortal CLI v", VERSION, PHP_EOL;
  echo "Usage: lcp_cli command [arguments]", PHP_EOL;
  echo PHP_EOL;
  echo "commands:", PHP_EOL;
  echo "  start", PHP_EOL;
  echo "    - start captive portal service", PHP_EOL;
  echo "  stop", PHP_EOL;
  echo "    - stop captive portal service", PHP_EOL;
  echo "  restart", PHP_EOL;
  echo "    - restart captive portal service", PHP_EOL;
  echo "  json", PHP_EOL;
  echo "    - output the setting details as json", PHP_EOL;
  echo "  status", PHP_EOL;
  echo "    - print service status", PHP_EOL;
  echo "  enable", PHP_EOL;
  echo "    - enable and start captive portal service", PHP_EOL;
  echo "  disable", PHP_EOL;
  echo "    - stop and disable captive portal service", PHP_EOL;
  echo "  get", PHP_EOL;
  echo "    ssid", PHP_EOL;
  echo "    channel", PHP_EOL;
  echo "    redirect_url", PHP_EOL;
  echo "    auto_disconnect", PHP_EOL;
  echo "    loading_img", PHP_EOL;
  echo "  set", PHP_EOL;
  echo "    ssid [value(string)]", PHP_EOL;
  echo "    channel [1-14(number)]", PHP_EOL;
  echo "      - default: 7", PHP_EOL;
  echo "    redirect_url [url(URLENCODED string)]", PHP_EOL;
  echo "    auto_disconnect [true/false(bool)]", PHP_EOL;
  echo "      - default: true", PHP_EOL;
  echo "    loading_img", PHP_EOL;
  echo "      - default: wait.png", PHP_EOL;
  echo "  version", PHP_EOL;
  echo PHP_EOL;
}

if (count($argv) < 2) {
  help();
  exit(0);
}

switch ($argv[1]) {
  case "start":
    start();
    break;

  case "stop":
    stop();
    break;

  case "restart":
    restart();
    break;

  case "enable":
    enable();
    break;

  case "disable":
    disable();
    break;

  case "status":
    status();
    break;

  case "json":
    echo json_conf_output(), PHP_EOL;
    break;

  case "get":
    if (count($argv) < 3) {
      help();
      exit(0);
    }
    switch ($argv[2]) {
      case "ssid":
        echo key_read_conf_file("ssid", "/etc/hostapd/hostapd.conf"), PHP_EOL;
        break;

      case "channel":
        echo key_read_conf_file("channel", "/etc/hostapd/hostapd.conf"), PHP_EOL;
        break;

      case "redirect_url":
        echo key_read_conf_file("redirect_url", "/opt/lba-captiveportal/lcp.conf"), PHP_EOL;
        break;

      case "auto_disconnect":
        echo key_read_conf_file("auto_disconnect", "/opt/lba-captiveportal/lcp.conf"), PHP_EOL;
        break;

      case "loading_img":
        echo key_read_conf_file("loading_img", "/opt/lba-captiveportal/lcp.conf"), PHP_EOL;
        break;
    }
    break;

  case "set":
    if (count($argv) < 4) {
      help();
      exit(0);
    }
    switch ($argv[2]) {
      case "ssid":
        echo key_write_conf_file("ssid", $argv[3], "/etc/hostapd/hostapd.conf"), PHP_EOL;
        restart();
        break;

     case "channel":
        echo key_write_conf_file("channel", $argv[3], "/etc/hostapd/hostapd.conf"), PHP_EOL;
        restart();
        break;

      case "redirect_url":
        echo key_write_conf_file("redirect_url", $argv[3], "/opt/lba-captiveportal/lcp.conf") ? "OK" : "ERROR", PHP_EOL;
        break;

      case "auto_disconnect":
        echo key_write_conf_file("auto_disconnect", $argv[3], "/opt/lba-captiveportal/lcp.conf") ? "OK" : "ERROR", PHP_EOL;
        break;

      case "loading_img":
        echo key_write_conf_file("loading_img", $argv[3], "/opt/lba-captiveportal/lcp.conf") ? "OK" : "ERROR", PHP_EOL;
        break;
    }
    break;

  case "version":
    echo VERSION, PHP_EOL;
    break;

  default:
    help();
}

function start() {
  shell_exec("systemctl start hostapd.service 2>&1");
}

function stop() {
  shell_exec("systemctl stop hostapd.service 2>&1");
}

function restart() {
  shell_exec("systemctl restart hostapd.service 2>&1");
}

function status() {
  echo shell_exec("systemctl status hostapd.service | grep Active");
}

function enable() {
  shell_exec("systemctl enable hostapd.service 2>&1");
  shell_exec("systemctl start hostapd.service 2>&1");
}

function disable() {
  shell_exec("systemctl stop hostapd.service 2>&1");
  shell_exec("systemctl disable hostapd.service 2>&1");
}

function json_conf_output() {
  $ssid = key_read_conf_file("ssid", "/etc/hostapd/hostapd.conf");
  $channel = key_read_conf_file("channel", "/etc/hostapd/hostapd.conf");
  $redirect_url = key_read_conf_file("redirect_url", "/opt/lba-captiveportal/lcp.conf");
  $auto_disconnect = key_read_conf_file("auto_disconnect", "/opt/lba-captiveportal/lcp.conf");
  $loading_img = key_read_conf_file("loading_img", "/opt/lba-captiveportal/lcp.conf");

  $json_data = array(
    "ssid" => $ssid,
    "channel" => $channel,
    "redirect_url" => $redirect_url,
    "auto_disconnect" => $auto_disconnect,
    "loading_img" => $loading_img
  );

  return json_encode($json_data);
}

function key_read_conf_file($key, $path) {
  $value = parse_ini_file($path, false, INI_SCANNER_TYPED)[$key];
  if (is_bool($value)) {
    $value = json_encode($value);
  }

  return $value;
}

function key_write_conf_file($key, $value, $path) {
  $conf = parse_ini_file($path, false, INI_SCANNER_TYPED);
  $conf[$key] = $value;

  return write_conf_file($conf, $path);
}

function write_conf_file($assoc_arr, $path) {
  $content = "";
  foreach ($assoc_arr as $key => $value) {
    if (is_bool($value)) {
      $value = json_encode($value);
    }
    $content .= $key.'='.$value.PHP_EOL;
  }

  $f = fopen($path, "w+");
  if ($f && @fwrite($f, $content)) {
    @fclose($f);

    return true;
  }

  return false;
}
?>
