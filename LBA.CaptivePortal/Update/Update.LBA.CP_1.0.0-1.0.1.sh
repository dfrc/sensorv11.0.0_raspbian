#!/bin/bash

echo "Update Captive Portal v1.0.0 -> v1.0.1"
echo ""
echo " - Copying files..."

sudo cp -rf LBA.CP_1.0.0-1.0.1/* /

echo "  :Done"

echo " - Setting up a new config..."

sudo lcp_cli set loading_img wait.png

echo "  :Done"

echo ""
echo " - Config JSON"

sudo lcp_cli json

echo ""
echo " - Update completed."
echo "  :No reboot required."
