#!/bin/bash

echo "Update Captive Portal v1.0.1 -> v1.0.2"
echo ""
echo " - Copying files..."

sudo cp -rf LBA.CP_1.0.1-1.0.2/* /

echo "  :Done"

echo ""
echo " - Config JSON"

sudo lcp_cli json

echo ""
echo " - Update completed."
echo "  :No reboot required."
