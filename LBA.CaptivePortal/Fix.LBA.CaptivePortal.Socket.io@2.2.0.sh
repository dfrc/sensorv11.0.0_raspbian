#!/bin/bash

echo "Fix Captive Portal - Socket.io issue"
echo ""
echo " - remove socket.io ..."

sudo npm -g uninstall socket.io

echo "  :Done"

echo ""
echo " - install socket.io@2.2.0 ..."

sudo npm -g install socket.io@2.2.0

echo ""
echo " - Fix completed."
echo "  :reboot required."
