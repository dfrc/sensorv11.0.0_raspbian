echo "This script will update the sensor utilities to the last version"
set -e
sudo cp -r /opt/download/lastversion/utils/* /opt/utils
sudo rm -r /opt/utils/ssh_key
/opt/utils/Crontab.sh

sudo chmod 777 /opt/versions.txt
java -jar /opt/utils/softwareupdate-1.0.0.jar
java -jar /opt/utils/sensortemperature-2.0.jar
java -jar /opt/utils/softwareversion-1.1.jar

echo "Sensor update" 
/opt/download/lastversion/utils/update-sensor.sh

java -jar /opt/utils/sendmessage-1.0.0.jar "Software Update had been completed"

/opt/utils/update-ssh.sh
