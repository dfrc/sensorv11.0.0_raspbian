# This script shall create an hotsopt from LiVEWALK sensor 
# Note that the password is part of the script 
# 
# 2020-09-26 Version 1.0.0
# 2020-11-21 Version 1.0.1 
#

sudo apt-get install nmap
IFNAME="wlan0"
CON_NAME="LiVEWALK"
nmcli con add type wifi ifname $IFNAME con-name $CON_NAME autoconnect yes ssid $CON_NAME
nmcli con modify $CON_NAME 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared
nmcli con modify $CON_NAME wifi-sec.key-mgmt wpa-psk
nmcli con modify $CON_NAME wifi-sec.psk "MyStrongHotspot%Pass"
nmcli con up $CON_NAME
nmcli connection show
