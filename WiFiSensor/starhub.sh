#!/bin/bash
set -e 
echo "Netplan to NetworkManager. 2020-09-30"

echo "-- APT update and upgrade  --"
sudo apt update
sudo apt upgrade -y

echo "-- APT install network-manager --"
sudo apt install -y net-tools network-manager

echo "-- Netplan to NetworkManager #1/2 --"
echo "    renderer: NetworkManager" | sudo tee -a /etc/netplan/50-cloud-init.yaml
sudo netplan apply

echo "-- Netplan to NetworkManager #2/2 --"
sudo tee /etc/netplan/50-cloud-init.yaml > /dev/null <<EOT
# This file is generated from information provided by the datasource.  Changes
# to it will not persist across an instance reboot.  To disable cloud-init's
# network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}
network:
    version: 2
    renderer: NetworkManager

EOT

sudo netplan apply

echo "Netplan to NetworkManager : OK"
echo "No reboot required"

sudo nmcli connection add type gsm ifname '*' con-name "STARHUB-SG" apn "shppd"

sudo nmcli c


