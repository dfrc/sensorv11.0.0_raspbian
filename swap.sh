#!/bin/bash


echo "Configuring swap 2G"

# For ubuntu
# sudo fallocate -l 2G /swapfile
# sudo chmod 600 /swapfile
# sudo mkswap /swapfile
# sudo swapon /swapfile
# sudo cp /etc/fstab /etc/fstab.bak
# sudo cp fstab /etc/fstab
# sudo free -h

# For Raspbian OS
#!/bin/bash
sudo dphys-swapfile swapoff
sudo sed -i 's/CONF_SWAPSIZE=100/CONF_SWAPSIZE=2048/g' /etc/dphys-swapfile
sudo dphys-swapfile setup
sudo dphys-swapfile swapon
echo "Done"

#sudo echo "vm.swappiness=10" >> /etc/sysctl.conf
#sudo echo "vm.vfs_cache_pressure=50" >> /etc/sysctl.conf


