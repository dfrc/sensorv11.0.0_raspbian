# New installer for version 9.5.2
# Last update 05.8.2021
# For Rasbian OS


#!/bin/bash
sudo apt install git net-tools -y
# For .ssh directory
mkdir /home/pi/.ssh

set -e
source ./color-echo.sh
source versions.txt

echo "we do not update eeprom"
#./rpi-eeprom-update-autoconf.sh

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get update
sudo apt install unzip -y
sudo apt install speedtest-cli -y
sudo apt install ufw -y
# WiFi network 
sudo apt install wpasupplicant -y 
sudo apt install network-manager -y
# Serial port 
sudo apt-get install librxtx-java -y


echo "LBASense - Sensor Installation Version 9.4.8 - Only Sensor"

cecho "Version ${Version//v}" $cyan
if [[ $USER == root ]]; then
   cecho "This script must be run not root" $red 1>&2
   exit 1
fi

echo $USER
sudo pwd

#model=$(cat /proc/cpuinfo | grep Raspberry)
machine=$(uname -m)
echo $machine

cecho "--Update--" $yellow


if [ "$(uname -m | grep x86_64)" ]
then
  cecho "--Fix sudo command for Ubuntu x86_64 --" $yellow
  echo "$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers
  cecho "--DONE--" $blue
fi

if [ "$(cat /proc/cpuinfo | grep Raspberry)" ]
then
  cecho "--Installing Swap --" $yellow
  ./swap.sh
  cecho "--DONE--" $blue
fi

cecho "--Installing JAVA --" $yellow
sudo apt-get install default-jdk -y
cecho "--DONE--" $blue


cecho "--Installing ESP Sensor --" $yellow
./esp8266.sh
cecho "--DONE--" $blue

cecho "--Installing the GPS Module --" $yellow
./setup_gps.sh
cecho "--DONE--" $blue


#
# The following section had been removed 
# This section shall be replaces with OpenVPN 
#
#
#./reverse_ssh_tunnel-autoconf.sh
#

if [ "$(cat /proc/cpuinfo | grep Raspberry)" ]
then
  cecho "--Installing LED --" $yellow
  ./ledboard-autoconf.sh
  cecho "--DONE--" $blue
fi

cecho "--Store the software version under /opt/versions.txt --" $yellow
sudo cp versions.txt /opt
cecho "--DONE--" $blue

cecho "--executing utils.sh  --" $yellow
./utils.sh
cecho "--DONE--" $blue

cecho "--executing Crontab.sh  --" $yellow
/opt/utils/Crontab.sh
cecho "--DONE--" $blue

#
# This section should be replaced with OpenVPN
#
#

cecho "-- setup reverese ssh connection with RSSH  --" $yellow
./reverse_ssh_tunnel-autoconf.sh
cecho "--DONE--" $blue

#
# no captive portal - sensor only
#
#cecho "-- Updating Captive Portal --" $yellow
#cd LBA.CaptivePortal/
#./LBA.CaptivePortal.AutoConf.sh
#cecho "--DONE (with captive portal)--" $blue


cecho "-- initializing connection with RSSH  Server --" $yellow
set +epa
/opt/utils/event_backup.sh
set -e
cecho "--DONE--" $blue

cecho "-- Updating Reverse SSH port --" $yellow
/opt/utils/update_reverse_ssh.py --RemoteForward True
cecho "--DONE--" $blue


cecho "-- Registering a service for activation of the firewall after reboot --" $yellow
sudo cp /opt/utils/setup-firewall.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable setup-firewall.service
cecho "--DONE--" $blue

cecho "-- Update SSH ports --" $yellow
/opt/utils/update-ssh.sh
cecho "--DONE--" $blue

cecho "-- Please Reboot manually --" $yellow

