#!/bin/bash

# source ./color-echo.sh

# Install certbot and route53 plugin

echo "--Installing certbot --"
sudo snap install core; sudo snap refresh core
sudo apt-get remove certbot
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo snap set certbot trust-plugin-with-root=ok
sudo snap install certbot-dns-route53

# Setup AWS for certbot-dns-route53
sudo bash -c 'mkdir $HOME/.aws'
sudo bash -c 'cat <<EOF >>$HOME/.aws/credentials
[default]
aws_access_key_id=AKIAQM57R4XN3VF2VKL4
aws_secret_access_key=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
EOF'

echo "--Requesting SSL certificate --"
sudo certbot certonly -d *.registration.lbasense.com --dns-route53 -m dev@dfrc.kr --agree-tos --non-interactive --server https://acme-v02.api.letsencrypt.org/directory

# Setup NodeJS environment
echo "--Installing NodeJS and PM2 --"
curl -sL https://deb.nodesource.com/setup_15.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt-get install nodejs -y
sudo apt-get install build-essential -y
sudo npm install -g pm2

echo "--Setting up LbaWebsocketRegistration project --"
cd /opt
sudo git clone https://ernestinedfrc:xn99tnbr7h5SpnJGT2tM@bitbucket.org/dfrc/lba_websocket_registration.git


# To be replaced with another script to get siteId, user, pass
sudo bash -c 'cat <<EOF >>/opt/lba_websocket_registration/site-config.yaml
siteId: 4681
user: "lgreg_user"
pass: "Lgregistration01!"
encrypt: true
EOF'

cd /opt/lba_websocket_registration
sudo npm install

cd $HOME

sudo pm2 startOrRestart /opt/lba_websocket_registration/ecosystem.config.js --env production
sudo pm2 startup
sudo pm2 save
