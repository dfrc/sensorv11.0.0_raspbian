#!/bin/bash

# Run as User:pi

sudo apt-get update && sudo apt-get upgrade -y
sudo apt install -y raspberrypi-kernel-headers git libgmp3-dev gawk qpdf bison flex make
sudo apt install -y build-essential libtool automake texinfo
sudo apt install -y bluez mariadb-server mariadb-client

# install tools for python
sudo apt-get install -y python3-pip python-dev libatlas-base-dev libxslt-dev daemontools
pip3 install pandas pymysql pyshark nest_asyncio # (When using linux tshark's stdout)
sudo pip3 install pandas pymysql pyshark nest_asyncio pyserial

# install sniffing tools
sudo apt install -y tcpdump aircrack-ng pcapfix tshark

# Downgrade kernel to 5.4
# wget https://archive.raspberrypi.org/debian/pool/main/r/raspberrypi-firmware/raspberrypi-kernel_1.20200902-1_armhf.deb
# wget https://archive.raspberrypi.org/debian/pool/main/r/raspberrypi-firmware/raspberrypi-kernel-headers_1.20200902-1_armhf.deb
sudo apt remove --purge raspberrypi-kernel-headers -y
sudo dpkg -i raspberrypi-kernel_1.20200902-1_armhf.deb
sudo dpkg -i raspberrypi-kernel-headers_1.20200902-1_armhf.deb



# Reboot to apply downgraded kernel (5.4)
sudo reboot

