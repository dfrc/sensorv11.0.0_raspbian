#!/bin/bash

# Run as a Super user (sudo su)
cd /
git clone https://github.com/seemoo-lab/nexmon.git
cd /nexmon/buildtools/isl-0.10
./configure
make
make install
ln -s /usr/local/lib/libisl.so /usr/lib/arm-linux-gnueabihf/libisl.so.10


cd /nexmon/buildtools/mpfr-3.1.4
autoreconf -f -i
./configure
make
make install
ln -s /usr/local/lib/libmpfr.so /usr/lib/arm-linux-gnueabihf/libmpfr.so.4

# start test from below

cd /nexmon
source setup_env.sh
make
cd /nexmon/patches/bcm43455c0/7_45_206/nexmon/
make
make backup-firmware
make install-firmware

cd /nexmon/utilities/nexutil/
make && make install
apt-get remove -y wpasupplicant

# Set modified firwmare as a default firwmare (Modified firmware stays even after the reboot)
sudo mv /lib/modules/5.4.51-v7l+/kernel/drivers/net/wireless/broadcom/brcm80211/brcmfmac/brcmfmac.ko /lib/modules/5.4.51-v7l+/kernel/drivers/net/wireless/broadcom/brcm80211/brcmfmac/brcmfmac.ko.orig
sudo cp /nexmon/patches/bcm43455c0/7_45_206/nexmon/brcmfmac_5.4.y-nexmon/brcmfmac.ko /lib/modules/5.4.51-v7l+/kernel/drivers/net/wireless/broadcom/brcm80211/brcmfmac/
sudo depmod -a

rfkill unblock 2
# install sniffing tools
# apt install -y tcpdump aircrack-ng pcapfix
sudo airmon-ng check kill

# apt install -y tshark # Optional



