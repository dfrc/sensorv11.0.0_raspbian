import pandas as pd
import io
import os
import sys
import time
import datetime
import pymysql
import numpy as np
import re
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import warnings
#from start_WiFi_sensor import host, port, user, passwd, database_name
from cluster_event import *
import json
# configuration
current_path = os.path.dirname(os.path.abspath(__file__))
with open(f"{current_path}/conf/WiFi_sensor_config.json", "r") as conf:
    config_json = json.load(conf)
warnings.simplefilter(action='ignore', category=FutureWarning)
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Env
hours = 0 # For adjusting timezone
pcap_path = current_path + "/detection/current"

database_name = config_json['database_name']
network = config_json['network']
sensor_id = config_json['sensor_id']
sensor_passwd = config_json['sensor_passwd']
#allow_subtype_list = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,46,47]
allow_subtype_list = config_json['allow_subtype_list']
removal_mac_list = config_json['removal_mac_list']
valid_timewindow_seconds = config_json['valid_timewindow_s']
margin_oneside_second = config_json['margin_oneside_ms'] / 1000
overwrite_wlan_sa = config_json['overwrite_wlan_sa']
version = config_json['version']

# Network config for the database settings
# laptop, minipc, dev
if network == 'dev':
# DEV_new (External network)
    host = "116.38.162.38"
    port = 33306
    user = 'changun'
    passwd = 'dfrc2021'
elif network == 'laptop':
    # Yoga Laptop (Intranet)
    host = "192.168.0.2"
    port = 3306
    user = 'root'
    passwd = 'dfrc2021'
elif network == 'minipc':
    host = '192.168.0.100'
    port = 3306
    user = 'root'
    passwd = '5fgp3h84'
elif network == 'localhost':
    host = 'localhost'
    port = 3306
    user = 'root'
    passwd = '5fgp3h84'

def get_local_ip():
    import socket
    """Try to determine the local IP address of the machine."""
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # Use Google Public DNS server to determine own IP
        sock.connect(('8.8.8.8', 80))

        return sock.getsockname()[0]
    except socket.error:
        try:
            return socket.gethostbyname(socket.gethostname())
        except socket.gaierror:
            return '127.0.0.1'
    finally:
        sock.close() 

# Connect to DB
def connect_db(database, host=host, port=port, user=user, passwd=passwd):
    conn = pymysql.connect(
        host = host,
        port = port,
        user = user,
        passwd = passwd,
        database = database
    )
    curr = conn.cursor(pymysql.cursors.DictCursor)
    return conn,curr

def create_event_table(table_name):
    conn, curr = connect_db(database_name)
    sql = """
        CREATE TABLE IF NOT EXISTS {table_name} 
        (
            id INT AUTO_INCREMENT PRIMARY KEY,
            frame_number INT,
            frame_time DATETIME,
            frame_timestamp_ns BIGINT,
            wlan_sa TEXT,
            wlan_sa_resolved TEXT,
            frame_len INT,
            wlan_radio_signal_dbm INT,
            wlan_seq INT,
            wlan_radio_channel INT,
            wlan_ds_current_channel INT,
            wlan_ssid TEXT,
            wlan_tag_length TEXT,
            wlan_fc_type_subtype INT,
            wlan_fc TEXT,
            root_wlan_sa TEXT,
            j INT,
            Randomized TINYINT
        )
    """.format(table_name=table_name)
    curr.execute(sql)
    conn.close()

# fields -e frame.number -e frame.time -e wlan.sa -e wlan.sa_resolved -e frame.len -e wlan_radio.signal_dbm -e wlan.seq 
# -e wlan_radio.channel -e wlan.ds.current_channel -e wlan.ssid -e wlan.tag.length -e wlan.fc.type_subtype -e wlan.fc

def load_event_log(pcap_path, hours, allow_subtype=None, only_same_channel=True, removal_mac_list=removal_mac_list):
    try:
        start_time = time.time()
        df_columns = ['frame_number','frame_time','wlan_sa','wlan_sa_resolved','frame_len','wlan_radio_signal_dbm',
        'wlan_seq','wlan_radio_channel','wlan_ds_current_channel','wlan_ssid','wlan_tag_length','wlan_fc_type_subtype','wlan_fc']
        
        with open(pcap_path, 'r') as f:
            lines_list = f.readlines()
            lines_only_tap_12_list = [x for x in lines_list if x.count('\t')==12]
        lines_only_tap_12_str = '\n'.join(lines_only_tap_12_list)
        lines_buffer = io.StringIO(lines_only_tap_12_str)
        event_data_df = pd.read_csv(lines_buffer, header=None, sep="\t", error_bad_lines=False)[:-1]

        if len(event_data_df) == 0:
            print("There's no data in DataFrame (length of event_data_df == 0)")
            return pd.DataFrame()
        event_data_df.columns = df_columns
        def extract_frame_number(x):
            try:
                return re.findall(r"@\w+ (\w+)", x)[0]
            except:
                return None
        def convert_to_int(x):
            try:
                return int(x)
            except:
                return -1
        event_data_df = event_data_df.dropna(subset=['frame_time','wlan_sa','wlan_fc_type_subtype'])
        event_data_df.loc[:,'frame_number'] = event_data_df['frame_number'].apply(lambda x: extract_frame_number(x))
        event_data_df = event_data_df[event_data_df['frame_number'].astype(str).str.contains(r'\d')].reset_index(drop=True)
        event_data_df.loc[:,'frame_time'] = pd.to_datetime(event_data_df['frame_time'].apply(lambda x: x[:-4]), format="%b %d, %Y %H:%M:%S.%f")
        event_data_df['frame_timestamp_ns'] = event_data_df.loc[:,'frame_time'].astype(np.int64)
        event_data_df.loc[:, 'wlan_fc_type_subtype'] = event_data_df['wlan_fc_type_subtype'].apply(lambda x: convert_to_int(x))
        event_data_df = event_data_df[event_data_df['wlan_fc_type_subtype']!=-1].reset_index(drop=True)
        if allow_subtype != None:
            event_data_df = event_data_df[event_data_df['wlan_fc_type_subtype'].isin(allow_subtype)].reset_index(drop=True)
        # Remove raspberry pi signals
        event_data_df = event_data_df[~event_data_df['wlan_sa_resolved'].str.contains('Raspberr')].reset_index(drop=True)
        # Apply removal list
        if removal_mac_list != []:
            event_data_df = event_data_df[~event_data_df['wlan_sa'].isin(removal_mac_list)].reset_index(drop=True)
        event_data_df.loc[:, 'frame_time'] = event_data_df['frame_time'] + datetime.timedelta(hours=hours)
        if only_same_channel:
            event_data_df = event_data_df[event_data_df['wlan_radio_channel'].astype(float)==event_data_df['wlan_ds_current_channel'].astype(float)].reset_index(drop=True)
        event_data_df = event_data_df.where(event_data_df.notnull(), None)
        return event_data_df
    except Exception as e:
        if e.args[0] == 'No columns to parse from file':
            print(f"current file is empty.")
            return pd.DataFrame()
        else:
            raise e

def generate_event_data(local_ip, table_name, pcap_path, hours, allow_subtype=allow_subtype_list, keep_hours=24, save_only_last=False, prev_df_minutes=10, valid_timewindow_seconds=45, margin_oneside_second=0.15):
    # CREATE TABLE IF NOT EXISTS
    create_event_table(table_name)

    # Load event data from tshark file
    event_data_df = load_event_log(pcap_path, hours, allow_subtype)
    # When event_data_df is empty
    if len(event_data_df) == 0:
        print(f"event_data_df is empty")
        return pd.DataFrame()
    
    start_time = time.time()
    conn, curr = connect_db(database_name)
    res = curr.execute("""SELECT * FROM {table_name} ORDER BY id DESC LIMIT 1""".format(table_name=table_name))
    # When there's no stored data in the DB
    if res == 0:
        last_timestamp_ns = 0
        prev_event_df = pd.DataFrame()
    else:
        fetched_df = pd.DataFrame(curr.fetchall())
        last_timestamp_ns = fetched_df['frame_timestamp_ns'].values[0]
        # Fetch previous event data to cluster
        prev_start_time = datetime.datetime.now() - datetime.timedelta(minutes=prev_df_minutes)
        curr.execute("""SELECT * FROM {table_name} WHERE frame_time >= %s""".format(table_name=table_name), prev_start_time)
        prev_event_df = pd.DataFrame(curr.fetchall())
        if len(prev_event_df) == 0:
            prev_event_df = pd.DataFrame()
        else:
            prev_event_df = prev_event_df.drop('id', axis=1)
            prev_event_df.loc[:,'frame_time'] = pd.to_datetime(prev_event_df['frame_timestamp_ns'])
    filtered_event_data_df = event_data_df[event_data_df['frame_timestamp_ns']>last_timestamp_ns].reset_index(drop=True)

    not_random_mac_list = get_not_random_mac_list(prev_event_df.append(filtered_event_data_df, ignore_index=True))
    filtered_event_data_df, merged_df = inherit_clustering(prev_event_df, filtered_event_data_df, not_random_mac_list, margin_oneside_second=margin_oneside_second, valid_timewindow_seconds=valid_timewindow_seconds)

    # For truncating
    res2 = curr.execute(f"""SELECT frame_time FROM {table_name} ORDER BY id LIMIT 1""")
    if res2 == 0:
        first_datetime = datetime.datetime.now()
    else:
        first_datetime = curr.fetchall()[0]['frame_time']

    # Truncate table if it's too big
    if first_datetime <= datetime.datetime.now() - datetime.timedelta(hours=keep_hours):
        curr.execute(f"""TRUNCATE TABLE {table_name}""")
        print(f"{table_name} has been truncated. | keep_hours: {keep_hours}")

    # Save filtered_event_data_df
    event_data_df_columns = list(filtered_event_data_df.columns)
    if save_only_last == True:
        temp_filtered_event_data_df = filtered_event_data_df.tail(1)
    else:
        temp_filtered_event_data_df = filtered_event_data_df
    len_df = len(temp_filtered_event_data_df)
    start_time = time.time()
    for row_tup in temp_filtered_event_data_df.iterrows():
        index = row_tup[0]
        row = row_tup[1]
        row = row.where(row.notnull(), None)
        sql = """INSERT INTO {table_name} 
            ({table_columns})
            VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
        """.format(table_name=table_name, table_columns=','.join(event_data_df_columns))
        val = [row[x] for x in event_data_df_columns]
        curr.execute(sql, val)
    conn.commit()
    conn.close()
    print_str = f"{local_ip} | DB | {len_df} row(s) have been sent to {host} | {datetime.datetime.now()}"
    print(print_str)
    return filtered_event_data_df

def convert_pi_event_to_esp(sensor_id, df, hours=0):
    if len(df) == 0:
        print(f"Nothing has been sent to the server (Empty DataFrame)")
        return
    start_time = time.time()
    esp_columns = ['sensor_id','unknown','visitor_id','globalMacFlag','routerMacFlag','manufacturerMac6Hex','manufacturerMac7Hex','manufacturerMac9Hex','randomizationFlag','routerFlag','softwareVersion','encryptionType_id','keyVersion','WIFI_EVENT','RSSI','dsStatus_not_used','channel_not_used','seqNumber_not_used','fragNumber_not_used','LENGTH_not_used','timestamp','SSID','frameControlHex']
    converted_df = pd.DataFrame(columns=esp_columns)

    def temp(x):
        try:
            res = int(x.replace(':',''), 16)
        except:
            res = None
        return res
    converted_df['visitor_id'] = df['wlan_sa'].apply(lambda x: temp(x))
    converted_df['sensor_id'] = sensor_id
    converted_df['unknown'] = 0
    converted_df['globalMacFlag'] = df['wlan_sa'].str.replace(':','').apply(lambda x: 1 if int(x[:2], 16)&2 == 0 else 0)
    converted_df['routerMacFlag'] = df['wlan_fc_type_subtype'].apply(lambda x: 1 if str(x) in ['5','8'] else 0)
    converted_df['manufacturerMac6Hex'] = df['wlan_sa'].str.replace(':','').apply(lambda x: int(x[:6],16))
    converted_df['manufacturerMac7Hex'] = df['wlan_sa'].str.replace(':','').apply(lambda x: int(x[:7],16))
    converted_df['manufacturerMac9Hex'] = df['wlan_sa'].str.replace(':','').apply(lambda x: int(x[:9],16))
    converted_df['randomizationFlag'] = 0 # todo?
    converted_df['routerFlag'] = df['wlan_fc_type_subtype'].apply(lambda x: 1 if str(x) in ['5','8'] else 0)
    converted_df['softwareVersion'] = 0
    converted_df['encryptionType_id'] = 0
    converted_df['keyVersion'] = 0
    converted_df['WIFI_EVENT'] = 2
    converted_df['RSSI'] = df['wlan_radio_signal_dbm']
    converted_df['dsStatus_not_used'] = df['wlan_ds_current_channel'].fillna(0).astype(int)
    converted_df['channel_not_used'] = df['wlan_radio_channel']
    converted_df['seqNumber_not_used'] = df['wlan_seq'].astype(int)
    converted_df['fragNumber_not_used'] = 0
    converted_df['LENGTH_not_used'] = df['frame_len']
    converted_df['timestamp'] = df['frame_timestamp_ns'].apply(lambda x: int(str(x)[:-3]))
    if hours < 0:
        converted_df.loc[:, 'timestamp'] = (converted_df['timestamp'] - int(datetime.timedelta(hours=hours).total_seconds() * 1000000))
    else:
        converted_df.loc[:, 'timestamp'] = (converted_df['timestamp'] + int(datetime.timedelta(hours=hours).total_seconds() * 1000000))
    converted_df['SSID'] = df['wlan_ssid']
    converted_df['frameControlHex'] = df['wlan_fc'].apply(lambda x: x[6:8])
    converted_df = converted_df.where(converted_df.notnull(), None)
    return converted_df


def send_to_EventReceiver(df, sensor_id, sensor_passwd, master_endpoint="https://master.lbasense.com/Sensor/Domain", extended=True, overwrite_wlan_sa=False):
    start_time = time.time()
    payload = {
    'sensorId': sensor_id,
    'pass': sensor_passwd
    }
    response = requests.get(master_endpoint, payload, verify=False)
    root_endpoint = re.findall(r"[\w\W]+/",response.json()['domain'])[0]
    EventReceiver_endpoint = root_endpoint + 'EventReceiver'
    if overwrite_wlan_sa:
        df = overwrite_wlan_sa_func(df)
    orig_converted_df = convert_pi_event_to_esp(sensor_id, df)
    sensor_id = int(orig_converted_df['sensor_id'].iloc[0])
    if extended == True:
        converted_df = orig_converted_df.iloc[:, 2:]
    else:
        converted_df = orig_converted_df.iloc[:, 2:-2]

    converted_df.loc[:,'SSID'] = converted_df['SSID'].fillna('NOSSID')
    events_list = []
    for row_tup in converted_df.iterrows():
        row = row_tup[1]
        events_list.append(','.join([str(x) for x in row.to_list()]))
    events_str = '\n'.join(events_list)
    payload = {
    'sensorID': sensor_id,
    'password': sensor_passwd,
    'time': int(time.time()),
    'events': events_str
    }
    requests.post(EventReceiver_endpoint, payload, verify=False)
    print_str = f"{len(converted_df)} row(s) have been sent to {EventReceiver_endpoint} | Sensor{sensor_id}"
    print(print_str)

    # Send StatusReport
    status_report_endpoint = f"{root_endpoint}StatusReport"
    status_report_headers = {
        'Content-Type': 'application/json'
    }
    status_report_body = {
        "id": sensor_id,
        "pass": sensor_passwd,
        "time": int(time.time()),
        "wifiEvents": len(converted_df),
        "wifiDevices": len(converted_df['visitor_id'].unique()),
        "uptime": int(float(os.popen("awk '{print $1}' /proc/uptime").read().strip())),
        "version": version
    }
    status_report_response = requests.post(status_report_endpoint, json.dumps(status_report_body), headers=status_report_headers, verify=False)
    if status_report_response.status_code != 200:
        print_str = f"Failed to send Status Report.\n" + status_report_response.text
        print(print_str)

def send_event_cycle(pcap_path, hours, valid_timewindow_seconds, margin_oneside_second, sensor_id, sensor_passwd, overwrite_wlan_sa=True):
    local_ip = get_local_ip()
    table_name = "pi_" + local_ip.replace('.', '_')
    while True:
        try:
            filtered_event_data_df = generate_event_data(local_ip, table_name, pcap_path, hours, keep_hours=72, save_only_last=False, prev_df_minutes=10, valid_timewindow_seconds=valid_timewindow_seconds, margin_oneside_second=margin_oneside_second)
            send_to_EventReceiver(filtered_event_data_df, sensor_id=sensor_id, sensor_passwd=sensor_passwd, overwrite_wlan_sa=overwrite_wlan_sa)
        except Exception as e:
            print_str = f"Error occured while sending event data"
            print(print_str)
            print(f"Error: {e}")
        time.sleep(10)
        
if __name__ == '__main__':
    send_event_cycle(pcap_path, hours, valid_timewindow_seconds, margin_oneside_second, sensor_id, sensor_passwd, overwrite_wlan_sa=True)
