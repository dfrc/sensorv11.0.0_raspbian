from multiprocessing import Process, Manager
import pandas as pd
import datetime
import numpy as np
import time

def cluster_event_pi(df, not_random_mac_list=None, pattern_second=5, margin_oneside_second=0.1, valid_timewindow_seconds=30, return_dict=None):
    # filter 1. Consider only rows that have detected in channel 6 # It's handled in load_event_log in send_event.py
    # filter 2. filter out global mac (wlan_sa_resolved != wlan_sa)
    # filter 3. consider only probe request (wlan_fc_type_subtype == 4)
    # df = df[df['wlan_ds_current_channel']==6] # It's handled in load_event_log in send_event.py
    resolved_mac_list = df[df['wlan_sa']!=df['wlan_sa_resolved']].drop_duplicates('wlan_sa')['wlan_sa'].to_list()
    # def timestamp_std_temp(x):
    #     d = {}
    #     d['timestamp_std'] = x.apply(lambda x: x.timestamp()).std()
    #     return pd.Series(d)
    # timestamp_std_df = df.groupby('wlan_sa')['frame_time'].apply(lambda x: timestamp_std_temp(x)).reset_index()
    # curr_not_random_mac_list = timestamp_std_df[timestamp_std_df['frame_time']>1.5]['wlan_sa'].to_list() + resolved_mac_list
    curr_not_random_mac_list = resolved_mac_list
    if not_random_mac_list != None:
        curr_not_random_mac_list += not_random_mac_list
    random_flag_df = (df[df['wlan_fc_type_subtype']==4]['wlan_sa'].value_counts() < 7).reset_index().rename(columns={'wlan_sa':'Randomized', 'index':'wlan_sa'})
    random_flag_df.loc[:,'Randomized'] = random_flag_df.apply(lambda x: False if x['wlan_sa'] in curr_not_random_mac_list else x['Randomized'], axis=1)
    random_mac_list = random_flag_df[random_flag_df['Randomized']==True]['wlan_sa'].to_list()
    probe_df = df[df['wlan_sa'].isin(random_mac_list)].drop_duplicates('wlan_sa', keep='first').reset_index(drop=True)
    if len(probe_df) <= 1:
        df['root_wlan_sa'] = None
        df['j'] = None
        df['Randomized'] = None
        if return_dict != None:
            return_dict['res'] = df
        return df
    # print(f"Length of unique probing request: {len(df[df['wlan_fc_type_subtype']==4].drop_duplicates('wlan_sa'))}")
    # print(f"Length of unique probing request from random mac: {len(probe_df)}")

    shift_num = len(probe_df) - 1

    concat_array = probe_df[['wlan_sa','frame_time']].to_numpy()
    start = time.time()
    # print(f"DataFrame concatenation start")
    concat_df_cols = ['wlan_sa','frame_time']

    def concat_pi(start_shift, end_shift, probe_df):
        try:
            concat_df_cols = []
            for i in range(start_shift+1, end_shift+1):
                shifted_probe_df = probe_df.shift(-i)
                shifted_wlan_sa = shifted_probe_df['wlan_sa']
                concat_df_cols.append(f"wlan_sa_shift{i}")
                shifted_wlan_sa_array = np.expand_dims(shifted_wlan_sa.to_numpy(), axis=1)

                shifted_length_check = shifted_probe_df['frame_len']==probe_df['frame_len']
                concat_df_cols.append(f"frame_len_equal_shift{i}")
                shifted_length_check_array = np.expand_dims(shifted_length_check.to_numpy(), axis=1)

                shifted_timestamp = shifted_probe_df['frame_time']
                concat_df_cols.append(f"timestamp_shift{i}")
                shifted_timestamp_array = np.expand_dims(shifted_timestamp.to_numpy(), axis=1)

                if i == start_shift+1:
                    concat_array = np.hstack([shifted_wlan_sa_array, shifted_length_check_array, shifted_timestamp_array])
                else:
                    concat_array = np.hstack([concat_array, shifted_wlan_sa_array, shifted_length_check_array, shifted_timestamp_array])
            return concat_array, concat_df_cols
        except Exception as e:
            print(f"Error occured in concat_pi func")
            print(f"Error: {e}")
            print(f"start_shift: {start_shift}, end_shift: {end_shift}")
            print(f"probe_df:{probe_df}")
            print(f"i: {i}")
            
    gen_concat_array, gen_concat_df_cols = concat_pi(0, shift_num, probe_df)
    concat_array = np.hstack([concat_array, gen_concat_array])
    concat_df_cols = concat_df_cols + gen_concat_df_cols
    concat_df = pd.DataFrame(concat_array, columns=concat_df_cols)
    # print(f"DataFrame concatenation finished: {time.time()-start}s")

    # Exctract col names for frame_len_equal_shift and within_error_margin
    frame_len_equal_shift_cols = [x for x in concat_df.columns if "frame_len_equal_shift" in x]

    # First condition -> Remove rows that don't have equal Length at all
    filter1 = (concat_df[frame_len_equal_shift_cols]).sum(axis=1) != 0

    # Helper function for clustering based on time intervals
    def validate_time_margin(seconds_delta, pattern_second, margin_oneside_second):
        quotient = seconds_delta // pattern_second
        first_condition = seconds_delta <= ((pattern_second+margin_oneside_second) * quotient) and quotient != 0
        second_condition = seconds_delta >= ((pattern_second-margin_oneside_second) * (quotient+1))
        # # For Debug
        # if first_condition:
        #     print(f"First_condition: Passed validation")
        # else:
        #     print(f"First_condition: Failed")
        # if second_condition:
        #     print(f"Second_condition: Passed validation")
        # else:
        #     print(f"Second_condition: Failed")
        valid_flag = first_condition or second_condition
        return valid_flag

    # Start clustering by processing each row
    start = time.time()
    # print(f"Clustering process started")
    cluster_info_df = pd.DataFrame(columns=['root_wlan_sa', 'clustered_wlan_sa'])
    for row_tup in concat_df[filter1].iterrows():
        row = row_tup[1]
        previous_timestamp = int(row['frame_time'].to_numpy())
        for j in range(1, len(frame_len_equal_shift_cols)+1):
            if row[f'timestamp_shift{j}'] == None:
                continue
            seconds_delta = (row[f'timestamp_shift{j}'] - previous_timestamp) / 1000000000
            valid_time_flag = validate_time_margin(seconds_delta, pattern_second, margin_oneside_second)
            if row[f"frame_len_equal_shift{j}"] and valid_time_flag:
                # Check the time window
                if seconds_delta <= valid_timewindow_seconds:
                    previous_timestamp = row[f'timestamp_shift{j}']
                    # Append it only if the mac is not clustered by other root mac
                    if row[f"wlan_sa_shift{j}"] not in cluster_info_df['clustered_wlan_sa'].to_list():
                        cluster_info_df = cluster_info_df.append({'root_wlan_sa': row['wlan_sa'], 'clustered_wlan_sa': row[f"wlan_sa_shift{j}"], 'j': j}, ignore_index=True)
    # print(f"Clustering process finished: {time.time()-start}s")

    result_df = df.merge(cluster_info_df, how='left', left_on='wlan_sa', right_on='clustered_wlan_sa')
    result_df = result_df.drop(['clustered_wlan_sa'], axis=1)
    result_df.loc[:, 'root_wlan_sa'] = result_df.loc[:, 'root_wlan_sa'].fillna(result_df['wlan_sa'])
    result_df = result_df.merge(random_flag_df, how='left', on='wlan_sa')
    def temp(row):
        if row['wlan_fc_type_subtype'] != 4:
            row['root_wlan_sa'] = None
            row['j'] = None
            row['Randomized'] = None
        return row
    result_df = result_df.apply(lambda x: temp(x), axis=1)
    result_df = result_df.where(result_df.notnull(), None)
    
    if return_dict != None:
        return_dict['res'] = result_df
    return result_df

def get_not_random_mac_list(df):
    def timestamp_std_temp(x):
        d = {}
        d['timestamp_std'] = x.apply(lambda x: x.timestamp()).std()
        return pd.Series(d)
    timestamp_std_df = df.groupby('wlan_sa')['frame_time'].apply(lambda x: timestamp_std_temp(x)).reset_index()
    return timestamp_std_df[timestamp_std_df['frame_time']>1.5]['wlan_sa'].to_list()

def overwrite_wlan_sa_func(df):
    df['wlan_sa'] = df.apply(lambda x: x['wlan_sa'] if x['root_wlan_sa']==None else x['root_wlan_sa'], axis=1)
    df['root_wlan_sa'] = df['wlan_sa']
    df = df.drop(['root_wlan_sa','j','Randomized'], axis=1)
    return df

def inherit_clustering(prev_event_df, curr_event_df, not_random_mac_list, margin_oneside_second=0.1, valid_timewindow_seconds=30):
    overlap_seconds = valid_timewindow_seconds

    if len(prev_event_df) == 0:
        inherited_curr_df = cluster_event_pi(curr_event_df, not_random_mac_list=not_random_mac_list, margin_oneside_second=margin_oneside_second, valid_timewindow_seconds=valid_timewindow_seconds)
        return inherited_curr_df, pd.DataFrame()
    # # restore previous event data to the original format
    # else:
    #     prev_event_df = overwrite_wlan_sa_func(prev_event_df)

    # Append old df to the current df according to the overlap_seconds
    first_datetime_curr_df = curr_event_df.head(1)['frame_time']
    filter_start_datetime = (first_datetime_curr_df-datetime.timedelta(seconds=overlap_seconds)).iloc[0]
    appended_curr_event_df = prev_event_df[prev_event_df['frame_time']>=filter_start_datetime].drop(['root_wlan_sa','j','Randomized'], axis=1).append(curr_event_df, ignore_index=True)

    # # Cluster each df with multiprocessing
    # manager = Manager()
    # prev_return_dict = manager.dict()
    # curr_return_dict = manager.dict()
    # prev_proc_kwargs = {
    #     'not_random_mac_list': not_random_mac_list,
    #     'margin_oneside_second': margin_oneside_second,
    #     'valid_timewindow_seconds': valid_timewindow_seconds,
    #     'return_dict': prev_return_dict
    # }
    # curr_proc_kwargs = {
    #     'not_random_mac_list': not_random_mac_list,
    #     'margin_oneside_second': margin_oneside_second,
    #     'valid_timewindow_seconds': valid_timewindow_seconds,
    #     'return_dict': curr_return_dict
    # }
    # prev_df_proc = Process(target=cluster_event_pi, args=(prev_event_df,), kwargs=prev_proc_kwargs)
    # prev_df_proc.start()
    # appended_curr_df_proc = Process(target=cluster_event_pi, args=(appended_curr_event_df,), kwargs=curr_proc_kwargs)
    # appended_curr_df_proc.start()

    # prev_df_proc.join()
    # appended_curr_df_proc.join()
    # prev_df = prev_return_dict['res']
    # appended_curr_df = curr_return_dict['res']
    appended_curr_df = cluster_event_pi(appended_curr_event_df, not_random_mac_list=not_random_mac_list, margin_oneside_second=margin_oneside_second, valid_timewindow_seconds=valid_timewindow_seconds)

    prev_clustered_df = prev_event_df[(prev_event_df['wlan_sa']!=prev_event_df['root_wlan_sa'])&(prev_event_df['wlan_fc_type_subtype']==4)].drop_duplicates('wlan_sa')
    prev_clustered_mac_list = prev_clustered_df['wlan_sa'].to_list()
    prev_clustered_root_mac_list = prev_clustered_df['root_wlan_sa'].to_list()
    prev_clustered_mac_dict = dict(zip(prev_clustered_mac_list, prev_clustered_root_mac_list))
    appended_curr_df.loc[appended_curr_df['wlan_fc_type_subtype']==4,'root_wlan_sa'] = appended_curr_df.loc[appended_curr_df['wlan_fc_type_subtype']==4,'root_wlan_sa'].replace(prev_clustered_mac_dict)
    
    # restore curr_df
    inherited_curr_df = appended_curr_df[appended_curr_df['frame_time']>=first_datetime_curr_df.iloc[0]].reset_index(drop=True)
  
    merged_df = prev_event_df.append(inherited_curr_df, ignore_index=True).drop_duplicates(['frame_timestamp_ns','wlan_sa'])
    return inherited_curr_df, merged_df
    