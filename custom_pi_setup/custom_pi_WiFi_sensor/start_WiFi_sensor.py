import datetime
import time
import pymysql
import os
import subprocess
import json
import sys
from send_event import *

# Input parameter flag
if len(sys.argv) == 1:
    input_parameter = None
else:
    input_parameter = sys.argv[1].lower()

# configuration
current_path = os.path.dirname(os.path.abspath(__file__))
with open(f"{current_path}/conf/WiFi_sensor_config.json", "r") as conf:
    config_json = json.load(conf)
mail_to = config_json['mail_to']
scanning_channel = config_json['scanning_channel']

# Connect to DB
def connect_db(database, host=host, port=port, user=user, passwd=passwd):
    conn = pymysql.connect(
        host = host,
        port = port,
        user = user,
        passwd = passwd,
        database = database
    )
    curr = conn.cursor(pymysql.cursors.DictCursor)
    return conn,curr

# Create database if not exist
def create_db_if_not_exists(database_name, host=host, port=port, user=user, passwd=passwd):
    conn = pymysql.connect(
        host=host,
        port=port,
        user=user,
        password=passwd
    )
    conn.cursor().execute(f"""CREATE DATABASE IF NOT EXISTS {database_name}""")
    conn.close()
    return

def get_local_ip():
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

def sendAutostartMail(mail_to="changun@dfrc.ch"):
    try:
        ip = get_local_ip()
        import smtplib
        from email.mime.text import MIMEText
        smtp = smtplib.SMTP('smtp.gmail.com', 587)
        smtp.ehlo()      # say Hello
        smtp.starttls()  # TLS 사용시 필요
        smtp.login('changun@dfrc.ch', '13245dh!!')
        
        msg = MIMEText(f'IP Address: {ip}')
        msg['Subject'] = '[Rpi4]: Boot message'
        msg['To'] = mail_to
        smtp.sendmail("changun@dfrc.ch", mail_to, msg.as_string())
        smtp.quit()
        return ip
    except Exception as e:
        print(f"Error occured in sendautostartMail func")
        print(f"Error: {e}")
        ip = get_local_ip()
        return ip

# def get_serial_number():
#     res = os.popen("cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2").read()
#     serial_number = res.strip()
#     return serial_number

# def register_ip_db(ip, serial_number):
#     conn, curr = connect_db(database_name)
#     curr.execute("""
#     CREATE TABLE IF NOT EXISTS ip_table
#     (
#         id INT AUTO_INCREMENT PRIMARY KEY,
#         datetime DATETIME,
#         serial_number TEXT,
#         ip TEXT,
#         tshark_scanning_channel INT,
#         remark TEXT
#     )
#     """)
#     res = curr.execute("""SELECT * FROM ip_table WHERE serial_number=%s""", serial_number)
#     if res == 0:
#         curr.execute("""INSERT INTO ip_table(datetime, serial_number, ip) VALUES(%s,%s,%s)""", (datetime.datetime.now()+datetime.timedelta(hours=9), serial_number, ip))
#         conn.commit()
#     else:1
#         curr.execute("""UPDATE ip_table SET datetime=%s, ip=%s WHERE serial_number=%s""", (datetime.datetime.now()+datetime.timedelta(hours=9), ip, serial_number))
#         conn.commit()
#     conn.close()

# def fetch_scanning_channel(serial_number, default_channel=6):
#     conn, curr = connect_db(database_name)
#     curr.execute("""SELECT tshark_scanning_channel FROM ip_table WHERE serial_number=%s""", serial_number)
#     fetched = curr.fetchall()
#     conn.close()
#     scanning_channel = fetched[0]['tshark_scanning_channel']
#     if scanning_channel == None:
#         print(f"Scanning_channle not configured... Scanning default channel: {default_channel}")
#         scanning_channel = default_channel
#     return scanning_channel

def manual_execute_tshark_python(dir, scanning_channel):
    p = subprocess.Popen([f"{dir}/start_tshark.sh"], stdout=subprocess.PIPE,stdin=subprocess.PIPE)
    time.sleep(0.1)
    p.stdin.write(f'{scanning_channel}\n'.encode())
    p.stdin.write(b'\n')
    p.stdin.close()
    time.sleep(3)
    os.system(f"{dir}/start_send_event.sh")

def service_execute_tshark_python(dir, scanning_channel):
    p = subprocess.Popen([f"{dir}/start_tshark.sh"], stdout=subprocess.PIPE,stdin=subprocess.PIPE)
    time.sleep(0.1)
    p.stdin.write(f'{scanning_channel}\n'.encode())
    p.stdin.write(b'\n')
    p.stdin.close()
    time.sleep(3)
    send_event_cycle(pcap_path, hours, valid_timewindow_seconds, margin_oneside_second, sensor_id, sensor_passwd, overwrite_wlan_sa=True)

if __name__ == "__main__": 
    create_db_if_not_exists(database_name)
    if mail_to != "":
        time.sleep(10)
        ip = sendAutostartMail(mail_to=mail_to)
    if input_parameter == "service":
        service_execute_tshark_python(current_path, scanning_channel)
    elif input_parameter == "manual":
        manual_execute_tshark_python(current_path, scanning_channel)
    else:
        print(f"Input parameter has not been specified.")
    # serial_number = get_serial_number()
    # register_ip_db(ip, serial_number)
    # scanning_channel = fetch_scanning_channel(serial_number, default_channel=6)

