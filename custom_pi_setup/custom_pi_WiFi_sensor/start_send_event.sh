#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Remove previous logs
cd $DIR
sudo rm nohup_send_event.out

stdbuf -oL nohup python3 send_event.py 2>&1 >> nohup_send_event.out &

# For generating PID
echo $! > $DIR/send_event.pid