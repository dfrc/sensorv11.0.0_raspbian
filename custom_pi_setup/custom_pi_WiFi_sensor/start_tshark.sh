#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
sudo chmod 777 $DIR
cd $DIR

# Remove previous logs
sudo rm -r detection
sudo rm nohup_tshark.out
sudo rm outfile_*

# Set Monitoring channel
echo -e "Set the monitoring channel: "
read input
# sudo airmon-ng check kill
sudo airmon-ng start wlan0 $input

nohup sudo tshark -i wlan0mon -t ad -b filesize:10000 -b files:1 -w outfile.pcapng -E header=y -T fields -e frame.number -e frame.time -e wlan.sa -e wlan.sa_resolved -e frame.len -e wlan_radio.signal_dbm -e wlan.seq -e wlan_radio.channel -e wlan.ds.current_channel -e wlan.ssid -e wlan.tag.length -e wlan.fc.type_subtype -e wlan.fc | multilog t s10000000 n1 '!tai64nlocal' ./detection 2>&1 >> nohup_tshark.out &

# For generating PID
echo $! > $DIR/tshark.pid