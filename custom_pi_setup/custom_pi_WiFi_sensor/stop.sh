#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
sudo kill -15 $(cat $DIR/tshark.pid) && echo "tshark has been killed."
sudo rm $DIR/tshark.pid && echo "tshark.pid has been removed."

sudo kill -15 $(cat $DIR/send_event.pid) && echo "send_event.py has been killed."
sudo rm $DIR/send_event.pid && echo "send_event.pid has been removed."
