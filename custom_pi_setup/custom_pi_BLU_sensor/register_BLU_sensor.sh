#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Check whether the script is executed by a root user.
if (($EUID==0)); then
    # echo "start.sh was executed by a root user."
    su - pi -c "python3 $DIR/conf/register_sensor.py"
    exit
else
    # echo "start.sh was executed by a non-root user."
    python3 $DIR/conf/register_sensor.py
    exit
fi