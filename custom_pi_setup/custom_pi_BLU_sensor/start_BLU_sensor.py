import datetime
import time
import pymysql
import os
import subprocess
import json
import sys
from send_event import *
# Input parameter flag
if len(sys.argv) == 1:
    input_parameter = None
else:
    input_parameter = sys.argv[1].lower()

# configuration
current_path = os.path.dirname(os.path.abspath(__file__))
with open(f"{current_path}/conf/BLU_sensor_config.json", "r") as conf:
    config_json = json.load(conf)
mail_to = config_json['mail_to']

# Connect to DB
def connect_db(database, host=host, port=port, user=user, passwd=passwd):
    conn = pymysql.connect(
        host = host,
        port = port,
        user = user,
        passwd = passwd,
        database = database
    )
    curr = conn.cursor(pymysql.cursors.DictCursor)
    return conn,curr

# Create database if not exist
def create_db_if_not_exists(database_name, host=host, port=port, user=user, passwd=passwd):
    conn = pymysql.connect(
        host=host,
        port=port,
        user=user,
        password=passwd
    )
    conn.cursor().execute(f"""CREATE DATABASE IF NOT EXISTS {database_name}""")
    conn.close()
    return

def get_local_ip():
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

def sendAutostartMail(mail_to="changun@dfrc.ch"):
    try:
        ip = get_local_ip()
        import smtplib
        from email.mime.text import MIMEText
        smtp = smtplib.SMTP('smtp.gmail.com', 587)
        smtp.ehlo()      # say Hello
        smtp.starttls()  # TLS 사용시 필요
        smtp.login('changun@dfrc.ch', '13245dh!!')
        
        msg = MIMEText(f'IP Address: {ip}')
        msg['Subject'] = '[Rpi4]: Boot message'
        msg['To'] = mail_to
        smtp.sendmail("changun@dfrc.ch", mail_to, msg.as_string())
        smtp.quit()
        return ip
    except Exception as e:
        print(f"Error occured in sendautostartMail func")
        print(f"Error: {e}")
        ip = get_local_ip()
        return ip

def manual_execute_tcpdump_python(dir):
    os.system(f"{dir}/start_tcpdump.sh")
    time.sleep(3)
    os.system(f"{dir}/start_send_event.sh")

def service_execute_tcpdump_python(dir):
    os.system(f"{dir}/start_tcpdump.sh")
    time.sleep(3)
    send_event_cycle(pcap_path, hours, sensor_id, sensor_passwd)

if __name__ == "__main__":
    time.sleep(10)
    create_db_if_not_exists(database_name)
    if mail_to != "":
        ip = sendAutostartMail(mail_to=mail_to)
    if input_parameter == "service":
        service_execute_tcpdump_python(current_path)
    elif input_parameter == "manual":
        manual_execute_tcpdump_python(current_path)
    else:
        print(f"Input parameter has not been specified.")

