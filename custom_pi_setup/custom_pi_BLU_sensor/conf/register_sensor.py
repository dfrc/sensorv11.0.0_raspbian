import re
import time
import os
import json
import requests
import warnings
from requests.packages.urllib3.exceptions import InsecureRequestWarning
warnings.simplefilter(action='ignore', category=FutureWarning)
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

user = 'changun'
user_pass = 'changun!'

current_path = os.path.dirname(os.path.abspath(__file__))
with open(f"{current_path}/BLU_sensor_config.json", "r") as conf:
    config_json = json.load(conf)

if config_json['sensor_id'] != 0:
    print(f"Sensor has already been registered in the master server (sensor_id != 0 in the config). Skipping registration.")
else:
    ip_add_str = os.popen("hciconfig").read()
    network_mac = re.findall(r"BD Address: (\w\w:\w\w:\w\w:\w\w:\w\w:\w\w)", ip_add_str)[0].lower()
    
    # Sensor registration
    sensor_registration_endpoint = f"https://master.lbasense.com/Sensor/Create?user={user}&pass={user_pass}"
    sensor_registration_headers = {
        'Content-Type': 'application/json'
    }
    sensor_registration_body = {
        "hardwareTypeId": 9, # Need to be fixed. For now, the API doesn't accept other types.
        "hardwareType": "ESP8266", # Need to be fixed. For now, the API doesn't accept other types.
        "macEth": network_mac,
        "macWlan": network_mac,
        "serverId": 98, # Associated server is set to Office-Core by default since DEV server is in progress of upgrade.
        "serverName": "Office-Core", # Associated server is set to Office-Core by default since DEV server is in progress of upgrade.
        "initialized": 0,
        "monitoringLagMinutes": 10,
        "description": "Custom Pi Bluetooth sensor",
        "sites": [],
        "status": "notInitialized",
        "softwareVersion": "0.0.0.0",
        "location": "LOCATION_TEST",
        "lastTimeSeen": 0
    }
    registration_response = requests.post(sensor_registration_endpoint, json.dumps(sensor_registration_body), headers=sensor_registration_headers, verify=False)
    if registration_response.status_code == 200:
        print(f"sensor{registration_response.json()['id']} has been successfully registered and associated to the {sensor_registration_body['serverName']} server.")
    else:
        print(f"Sensor registration has failed.")
        print(registration_response.text)
    
    # Sensor initialization
    sensor_initialization_endpoint = "https://master.lbasense.com/SensorConf"
    sensor_initialization_headers = {
        'Content-Type': 'application/json'
    }
    sensor_initialization_body = {
        "eth0": network_mac,
        "wlan0": network_mac,
        "hardware":"ESP8266", # Need to be fixed. For now, the API doesn't accept other types.
        "software":"0.0.0.0" # Temporary
    }
    initialization_response = requests.post(sensor_initialization_endpoint, json.dumps(sensor_initialization_body), headers=sensor_initialization_headers, verify=False)
    if initialization_response.status_code == 200:
        initialization_response_json = initialization_response.json()
        generated_sensor_id = initialization_response_json['id']
        print(f"Initialization for the sensor{generated_sensor_id} has been done.")
        generated_sensor_passwd = initialization_response_json['password']
        config_json['sensor_id'] = generated_sensor_id
        config_json['sensor_passwd'] = generated_sensor_passwd
        # Save generated sensor info into the config json file.
        with open(f"{current_path}/BLU_sensor_config.json", "w") as conf:
            json.dump(config_json, conf, indent=4)
        print(f"Sensor ID: {generated_sensor_id}, Sensor Password has been successfully saved in the config.")
    else:
        print(f"Initialization for the sensor{generated_sensor_id} failed.")
        print(initialization_response.text)

    # Fetch associated server domain
    get_domain_endpoint = "https://master.lbasense.com/Sensor/Domain"
    get_domain_response_json = requests.get(f"{get_domain_endpoint}?sensorId={generated_sensor_id}&pass={generated_sensor_passwd}").json()
    root_endpoint = re.findall(r"[\w\W]+/",get_domain_response_json['domain'])[0]

    # Send StatusReport for updating the timestamp
    status_report_endpoint = f"{root_endpoint}StatusReport"
    status_report_headers = {
        'Content-Type': 'application/json'
    }
    status_report_body = {
        "id": generated_sensor_id,
        "pass": generated_sensor_passwd,
        "time": int(time.time()),
        "wifiEvents": 0,
        "wifiDevices": 0,
        "uptime": int(float(os.popen("awk '{print $1}' /proc/uptime").read().strip())),
        "version": config_json['version']
    }
    status_report_response = requests.post(status_report_endpoint, json.dumps(status_report_body), headers=status_report_headers, verify=False)
    if status_report_response.status_code == 200:
        print(f"Status Report has been sent successfully.")
    else:
        print(f"Failed to send Status Report.")
        print(status_report_response.text)
    