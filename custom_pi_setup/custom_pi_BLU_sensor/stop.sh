#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
sudo kill -15 $(cat $DIR/tcpdump.pid) && echo "tcpdump has been killed."
sudo rm $DIR/tcpdump.pid && echo "tcpdump.pid has been removed."

sudo kill -15 $(cat $DIR/send_event.pid) && echo "send_event.py has been killed."
sudo rm $DIR/send_event.pid && echo "send_event.pid has been removed."
