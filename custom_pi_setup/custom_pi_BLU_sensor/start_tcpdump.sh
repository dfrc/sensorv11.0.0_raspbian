#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
sudo chmod 777 $DIR
cd $DIR

# Remove previous logs
sudo rm bluetooth.pcap
sudo rm nohup_bluetooth_scan.out

# start scanning bluetooth signals with bluetoothctl
sudo pkill -9 bluetoothctl
sudo bluetoothctl -- scan on > /dev/null &

# start dump with tcpdump
nohup sudo tcpdump -i bluetooth0 -w bluetooth.pcap -G 3600 2>&1 >> nohup_bluetooth_scan.out & # 1 hour rotation

# For generating PID
echo $! > $DIR/tcpdump.pid