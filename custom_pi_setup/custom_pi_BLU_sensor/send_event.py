import pandas as pd
import os
import subprocess
import time
import datetime
import pymysql
import pyshark
import numpy as np
import re
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import warnings
import json
import nest_asyncio
nest_asyncio.apply()
# configuration
current_path = os.path.dirname(os.path.abspath(__file__))
with open(f"{current_path}/conf/BLU_sensor_config.json", "r") as conf:
    config_json = json.load(conf)
warnings.simplefilter(action='ignore', category=FutureWarning)
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Env
hours = 0 # For adjusting timezone
bluetooth_pcap_file_name = 'bluetooth.pcap'
pcap_path = current_path + "/" + bluetooth_pcap_file_name

database_name = config_json['database_name']
network = config_json['network']
sensor_id = config_json['sensor_id']
sensor_passwd = config_json['sensor_passwd']
# removal_mac_list = config_json['removal_mac_list']
version = config_json['version']

# Network config for the database settings
# laptop, minipc, dev
if network == 'dev':
# DEV_new (External network)
    host = "116.38.162.38"
    port = 33306
    user = 'changun'
    passwd = 'dfrc2021'
elif network == 'laptop':
    # Yoga Laptop (Intranet)
    host = "192.168.0.2"
    port = 3306
    user = 'root'
    passwd = 'dfrc2021'
elif network == 'minipc':
    host = '192.168.0.100'
    port = 3306
    user = 'root'
    passwd = '5fgp3h84'
elif network == 'localhost':
    host = 'localhost'
    port = 3306
    user = 'root'
    passwd = '5fgp3h84'


def get_local_ip():
    import socket
    """Try to determine the local IP address of the machine."""
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # Use Google Public DNS server to determine own IP
        sock.connect(('8.8.8.8', 80))

        return sock.getsockname()[0]
    except socket.error:
        try:
            return socket.gethostbyname(socket.gethostname())
        except socket.gaierror:
            return '127.0.0.1'
    finally:
        sock.close() 

# Connect to DB
def connect_db(database, host=host, port=port, user=user, passwd=passwd):
    conn = pymysql.connect(
        host = host,
        port = port,
        user = user,
        passwd = passwd,
        database = database
    )
    curr = conn.cursor(pymysql.cursors.DictCursor)
    return conn,curr

def create_bluetooth_table(table_name):
    conn, curr = connect_db(database_name)
    sql = """
        CREATE TABLE IF NOT EXISTS {table_name} 
        (
            id INT AUTO_INCREMENT PRIMARY KEY,
            datetime DATETIME,
            timestamp BIGINT,
            packet_length INT,
            bd_addr TEXT,
            RSSI INT,
            device_name TEXT,
            company_id INT
        )
    """.format(table_name=table_name)
    curr.execute(sql)
    conn.close()

def load_bluetooth_pcap(pcap_path):
    p = subprocess.Popen(['sudo', 'pcapfix', pcap_path], stdout=subprocess.DEVNULL)
    p.wait()
    if p.returncode == 0:
        pass # put code that must only run if successful here.
    else:
        print(f"Error occured while fixing .pcap file with the pcapfix.")
    p.terminate()
    cap = pyshark.FileCapture(pcap_path.replace(bluetooth_pcap_file_name, "fixed_"+bluetooth_pcap_file_name))
    try:
        dict_list = []
        blue_dict = {}
        for i,packet in enumerate(cap):
            if int(packet.hci_h4.direction, 16) != 1:
                continue
            # blue_dict['i'] = i # For test
            blue_dict['timestamp'] = int(packet.sniff_timestamp[:-3].replace('.',''))
            blue_dict['packet_length'] = int(packet.length)
            if packet.get_multiple_layers('bthci_evt') != []:
                try:
                    blue_dict['bd_addr'] = packet.bthci_evt.bd_addr
                except:
                    # print(f"Couldn't find bd_addr from packet[{i}]")
                    blue_dict['bd_addr'] = None
                try:
                    blue_dict['RSSI'] = int(packet.bthci_evt.rssi)
                except:
                    # print(f"Couldn't find rssi from packet[{i}]")
                    blue_dict['RSSI'] = None
                try:
                    blue_dict['device_name'] = packet.bthci_evt.btcommon_eir_ad_entry_device_name
                except:
                    # print(f"Couldn't find device_name from packet[{i}]")
                    blue_dict['device_name'] = None
                try:
                    blue_dict['company_id'] = int(packet.bthci_evt.btcommon_eir_ad_entry_company_id)
                except:
                    # print(f"Couldn't find company_id from packet[{i}]")
                    blue_dict['company_id'] = None
                
            elif packet.get_multiple_layers('bthci_cmd') != []:
                try:
                    blue_dict['bd_addr'] = packet.bthci_cmd.bd_addr
                except:
                    # print(f"Couldn't find bd_addr from packet[{i}]")
                    blue_dict['bd_addr'] = None
                try:
                    blue_dict['RSSI'] = int(packet.bthci_evt.rssi)
                except:
                    # print(f"Couldn't find rssi from packet[{i}]")
                    blue_dict['RSSI'] = None
                try:
                    blue_dict['device_name'] = packet.bthci_cmd.btcommon_eir_ad_entry_device_name
                except:
                    # print(f"Couldn't find device_name from packet[{i}]")
                    blue_dict['device_name'] = None
                try:
                    blue_dict['company_id'] = int(packet.bthci_evt.btcommon_eir_ad_entry_company_id)
                except:
                    # print(f"Couldn't find company_id from packet[{i}]")
                    blue_dict['company_id'] = None
            else:
                print(f"packet[{i}] have neither of both bthci_evt layer and bthci_cmd layer")
            dict_list.append(blue_dict.copy())
        df = pd.DataFrame(dict_list)
        df['datetime'] = pd.to_datetime(df['timestamp'], unit='us')
        orig_cols = list(df.columns)
        modified_cols = ['datetime'] + [x for x in orig_cols if x != 'datetime']
        df = df[modified_cols]
        return df
    except Exception as e:
        if "TShark seems to have crashed" in e.args[0]:
            df = pd.DataFrame(dict_list)
            df['datetime'] = pd.to_datetime(df['timestamp'], unit='us')
            orig_cols = list(df.columns)
            modified_cols = ['datetime'] + [x for x in orig_cols if x != 'datetime']
            df = df[modified_cols]
            return df
        else:
            raise e

def generate_bluetooth_data(local_ip, table_name, pcap_path, keep_hours=24):
    # CREATE TABLE IF NOT EXISTS
    create_bluetooth_table(table_name)

    # Load bluetooth data from pcap file
    blue_df = load_bluetooth_pcap(pcap_path)
    # When blue_df is empty
    if len(blue_df) == 0:
        print(f"blue_df is empty")
        return pd.DataFrame()
    
    start_time = time.time()
    conn, curr = connect_db(database_name)
    res = curr.execute("""SELECT * FROM {table_name} ORDER BY id DESC LIMIT 1""".format(table_name=table_name))
    # When there's no stored data in the DB
    if res == 0:
        last_timestamp_us = 0
    else:
        fetched_df = pd.DataFrame(curr.fetchall())
        last_timestamp_us = fetched_df['timestamp'].values[0]
        
    # print(f"last_timestamp_us: {last_timestamp_us}")
    filtered_blue_df = blue_df[blue_df['timestamp']>last_timestamp_us].reset_index(drop=True)

    # For truncating
    res2 = curr.execute(f"""SELECT datetime FROM {table_name} ORDER BY id LIMIT 1""")
    if res2 == 0:
        first_datetime = datetime.datetime.now()
    else:
        first_datetime = curr.fetchall()[0]['datetime']

    # Truncate table if it's too big
    if first_datetime <= datetime.datetime.now() - datetime.timedelta(hours=keep_hours):
        curr.execute(f"""TRUNCATE TABLE {table_name}""")
        print(f"{table_name} has been truncated. | keep_hours: {keep_hours}")

    # Save filtered_blue_df
    blue_df_columns = list(filtered_blue_df.columns)
    len_df = len(filtered_blue_df)
    start_time = time.time()
    for row_tup in filtered_blue_df.iterrows():
        index = row_tup[0]
        row = row_tup[1]
        row = row.where(row.notnull(), None)
        sql = """INSERT INTO {table_name} 
            ({table_columns})
            VALUES(%s,%s,%s,%s,%s,%s,%s)
        """.format(table_name=table_name, table_columns=','.join(blue_df_columns))
        val = [row[x] for x in blue_df_columns]
        curr.execute(sql, val)
    conn.commit()
    conn.close()
    print(f"{local_ip} | {len_df} row(s) of Bluetooth detections have been sent to {host} | {datetime.datetime.now()}")
    return filtered_blue_df

def convert_bluetooth_event_to_esp(sensor_id, df, hours=0):
    # Remove rows that don't have bd_addr
    df = df[df['bd_addr'].notnull()]
    if len(df) == 0:
        print(f"Nothing has been sent to the server (Empty DataFrame)")
        return
    start_time = time.time()
    esp_columns = ['sensor_id','unknown','visitor_id','globalMacFlag','routerMacFlag','manufacturerMac6Hex','manufacturerMac7Hex','manufacturerMac9Hex','randomizationFlag','routerFlag','softwareVersion','encryptionType_id','keyVersion','WIFI_EVENT','RSSI','dsStatus_not_used','channel_not_used','seqNumber_not_used','fragNumber_not_used','LENGTH_not_used','timestamp','SSID','frameControlHex']
    converted_df = pd.DataFrame(columns=esp_columns)

    def temp(x):
        try:
            res = int(x.replace(':',''), 16)
        except:
            res = None
        return res
    converted_df['visitor_id'] = df['bd_addr'].apply(lambda x: temp(x))
    converted_df['sensor_id'] = sensor_id
    converted_df['unknown'] = 0
    converted_df['globalMacFlag'] = df['bd_addr'].str.replace(':','').apply(lambda x: 1 if int(x[:2], 16)&2 == 0 else 0)
    converted_df['routerMacFlag'] = 0 # ?
    converted_df['manufacturerMac6Hex'] = df['bd_addr'].str.replace(':','').apply(lambda x: int(x[:6],16))
    converted_df['manufacturerMac7Hex'] = df['bd_addr'].str.replace(':','').apply(lambda x: int(x[:7],16))
    converted_df['manufacturerMac9Hex'] = df['bd_addr'].str.replace(':','').apply(lambda x: int(x[:9],16))
    converted_df['randomizationFlag'] = 0 # todo?
    converted_df['routerFlag'] = 0 # ?
    converted_df['softwareVersion'] = 0
    converted_df['encryptionType_id'] = 0
    converted_df['keyVersion'] = 0
    converted_df['WIFI_EVENT'] = 1
    converted_df['RSSI'] = df['RSSI']
    converted_df['dsStatus_not_used'] = 0 # ?
    converted_df['channel_not_used'] = 0 # ?
    converted_df['seqNumber_not_used'] = 0 # ?
    converted_df['fragNumber_not_used'] = 0 # ?
    converted_df['LENGTH_not_used'] = df['packet_length']
    converted_df['timestamp'] = df['timestamp']
    if hours < 0:
        converted_df.loc[:, 'timestamp'] = (converted_df['timestamp'] - int(datetime.timedelta(hours=hours).total_seconds() * 1000000))
    else:
        converted_df.loc[:, 'timestamp'] = (converted_df['timestamp'] + int(datetime.timedelta(hours=hours).total_seconds() * 1000000))
    converted_df['SSID'] = df['device_name']
    converted_df['frameControlHex'] = 0
    converted_df = converted_df.where(converted_df.notnull(), None).reset_index(drop=True)
    return converted_df

def send_to_EventReceiver(df, sensor_id, sensor_passwd, master_endpoint="https://master.lbasense.com/Sensor/Domain", extended=True):
    start_time = time.time()
    payload = {
    'sensorId': sensor_id,
    'pass': sensor_passwd
    }
    response = requests.get(master_endpoint, payload, verify=False)
    root_endpoint = re.findall(r"[\w\W]+/",response.json()['domain'])[0]
    EventReceiver_endpoint = root_endpoint + 'EventReceiver'
    orig_converted_df = convert_bluetooth_event_to_esp(sensor_id, df)
    sensor_id = int(orig_converted_df['sensor_id'].iloc[0])
    if extended == True:
        converted_df = orig_converted_df.iloc[:, 2:]
    else:
        converted_df = orig_converted_df.iloc[:, 2:-2]

    converted_df.loc[:,'SSID'] = converted_df['SSID'].fillna('NOSSID')
    events_list = []
    for row_tup in converted_df.iterrows():
        row = row_tup[1]
        events_list.append(','.join([str(x) for x in row.to_list()]))
    events_str = '\n'.join(events_list)
    payload = {
    'sensorID': sensor_id,
    'password': sensor_passwd,
    'time': int(time.time()),
    'events': events_str
    }
    requests.post(EventReceiver_endpoint, payload, verify=False)
    print_str = f"{len(converted_df)} row(s) have been sent to {EventReceiver_endpoint} | Sensor{sensor_id}"
    print(print_str)

    # Send StatusReport
    status_report_endpoint = f"{root_endpoint}StatusReport"
    status_report_headers = {
        'Content-Type': 'application/json'
    }
    status_report_body = {
        "id": sensor_id,
        "pass": sensor_passwd,
        "time": int(time.time()),
        "wifiEvents": len(converted_df),
        "wifiDevices": len(converted_df['visitor_id'].unique()),
        "uptime": int(float(os.popen("awk '{print $1}' /proc/uptime").read().strip())),
        "version": version
    }
    status_report_response = requests.post(status_report_endpoint, json.dumps(status_report_body), headers=status_report_headers, verify=False)
    if status_report_response.status_code != 200:
        print_str = f"Failed to send Status Report.\n" + status_report_response.text
        print(print_str)

def send_event_cycle(pcap_path, hours, sensor_id, sensor_passwd):
    local_ip = get_local_ip()
    table_name = "pi_" + local_ip.replace('.', '_')
    while True:
        try:
            filtered_blue_df = generate_bluetooth_data(local_ip, table_name, pcap_path, keep_hours=72)
            send_to_EventReceiver(filtered_blue_df, sensor_id=sensor_id, sensor_passwd=sensor_passwd)
        except Exception as e:
            print(f"Error occured while sending Bluetooth data")
            print(f"Error: {e}")
        time.sleep(10)

if __name__ == '__main__':
    send_event_cycle(pcap_path, hours, sensor_id, sensor_passwd)