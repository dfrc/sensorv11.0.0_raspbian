#!/bin/bash
sudo cp ledboard/lbaled.dtbo /boot/overlays/
echo "dtoverlay=lbaled" | sudo tee -a /boot/config.txt

sudo mkdir /opt/lba_led/
sudo cp ledboard/led_control.sh /opt/lba_led/
sudo cp ledboard/hard_reset.sh /opt/lba_led/

crontab -l > lbacron
echo "* * * * * /opt/lba_led/led_control.sh" | tee -a lbacron
crontab lbacron
rm lbacron

sudo apt install build-essential -y

git clone https://github.com/WiringPi/WiringPi.git
cd WiringPi
./build
cd ..
rm -rf WiringPi

cat << EOF
---------------------------------

Reboot required.

[ LEDs ]
ls -al /sys/class/leds/
lba_led_d1 = server connected
lba_led_d2 = internet connected
lba_led_d3 = lan linked
lba_led_d4 = powered

[ HARD RESET ]
/opt/lba_led/hard_reset.sh

---------------------------------
EOF
