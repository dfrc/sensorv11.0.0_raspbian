#!/bin/bash
YELLOW='\033[1;33m'
RED='\033[0;31m'
BLUE='\033[1;34m'
SET='\033[0m'

echo "$--Installing pip and pyserial, numpy, pandas--$"
sudo apt install python3-pip libatlas-base-dev -y
sudo pip3 install pyserial numpy pandas

echo "$--Appending parameters into /boot/config.txt--$"
cat gps/add_to_config | sudo tee -a /boot/config.txt

echo "$--Removing 'console=serial0,115200' from /boot/cmdline.txt--$"
sudo sed -i 's/console=serial0,115200 //g' /boot/cmdline.txt
cat /boot/cmdline.txt

echo "$--GPS installation on the system--$"
sudo cp -r gps/lba_gps/ /opt/
ls -l /opt/lba_gps/

echo "$--Create service--$"
sudo cp gps/lba-gps.service /etc/systemd/system
sudo cp gps/lba-gps-core.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable lba-gps.service
sudo systemctl start lba-gps
sudo systemctl enable lba-gps-core.service
sudo systemctl start lba-gps-core
