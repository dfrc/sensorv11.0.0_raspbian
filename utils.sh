#!/bin/bash
set -e
source color-echo.sh

DIR_START=$(pwd)

cecho "--Add Utilities to /opt/utils --" $yellow
sudo cp -r utils/ /opt/

# .ssh is only avaliable on rb5
if [ "$(cat /proc/cpuinfo | grep Intel)" ]
then
  set +e
  mkdir ~/.ssh
  set -e
fi


sudo cp -r utils/ssh_key/* /home/pi/.ssh/
sudo chown pi /home/pi/.ssh/
sudo chmod 600 /home/pi/.ssh/*.pem
sudo chown pi /home/pi/.ssh/*.pem
sudo rm -rf utils/ssh_key
sudo rm -rf /opt/utils/ssh_key
cecho "--DONE--" $blue

